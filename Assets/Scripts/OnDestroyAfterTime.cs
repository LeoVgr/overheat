
using System.Collections;
using UnityEngine;

public class OnDestroyAfterTime : MonoBehaviour
{
    public float destroyTimer = 1f;

    public void DestroyChildrenAfterTime()
    {
        if(this.gameObject.activeInHierarchy)
            StartCoroutine("Despawn");
        //Destroy(gameObject, destroyTimer);
    }

    IEnumerator Despawn()
    {
        yield return new WaitForSeconds(destroyTimer);
        gameObject.SetActive(false);
    }
}
