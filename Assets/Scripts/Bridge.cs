using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bridge : MonoBehaviour
{
    public GameObject ColliderAfter;
    public GameObject ColliderBefore;
    public Animator Animator;
    public GameObject RobotSpawner;
    public GameObject RobotStoper;
    public GameObject RobotActiver;

    private bool _playerOn = false;
    private bool _closing = false;
    private void Start()
    {
        RobotSpawner.SetActive(true);
        RobotActiver.SetActive(true);
        RobotStoper.SetActive(false);
    }
    private void Update()
    {
        if (!this.GetComponentInParent<BridgeSound>().IsBridgyAllowed && !this.GetComponentInParent<BridgeSound>().IsSafeToClose)
        {
            print("EOOE");
            RobotSpawner.SetActive(true);
            RobotStoper.SetActive(false);
            RobotActiver.SetActive(true);
        }
        else
        {
            if (this.GetComponentInParent<BridgeSound>().IsUp)
            {
                ColliderAfter.SetActive(true);
                ColliderBefore.SetActive(true);
            }
            else
            {
                ColliderAfter.SetActive(false);
                ColliderBefore.SetActive(false);
            }

            if (this.GetComponentInParent<BridgeSound>().IsBridgyAllowed)
            {
                RobotSpawner.SetActive(true);
                RobotActiver.SetActive(true);
                RobotStoper.SetActive(false);
            }
            else
            {
                RobotSpawner.SetActive(false);
                RobotActiver.SetActive(false);
                RobotStoper.SetActive(true);
            }

            if (this.GetComponentInParent<BridgeSound>().IsSafeToClose && !_playerOn)
            {
                Animator.speed = 1f;
            }
            else
            {
                Animator.speed = 0f;
            }
        }      

    }
    private void OnTriggerEnter(Collider other)
    {      
        if (other.GetComponent<Laser>() && !this.GetComponentInParent<BridgeSound>().IsUp)
        {
            _playerOn = true;
            Animator.speed = 0f;
            RobotSpawner.SetActive(false);
            RobotActiver.SetActive(false);
            RobotStoper.SetActive(true);
        }
        
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Laser>() && !this.GetComponentInParent<BridgeSound>().IsUp)
        {
            _playerOn = false;
            Animator.speed = 1f;
            RobotSpawner.SetActive(true);
            RobotStoper.SetActive(false);
            RobotActiver.SetActive(true);
        }
    }

}
