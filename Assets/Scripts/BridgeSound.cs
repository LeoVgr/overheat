using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeSound : MonoBehaviour
{
    public bool IsUp = true;
    public bool IsBridgyAllowed = true;
    public bool IsSafeToClose = true;
    public AK.Wwise.Event UpSound;
    public AK.Wwise.Event DownSound;

    public void PlayUpSound()
    {
        UpSound.Post(this.gameObject);
    }
    public void PlayDownSound()
    {
        DownSound.Post(this.gameObject);
    }
    public void SetIsUp()
    {
        IsUp = true;
    }
    public void SetIsDown()
    {
        IsUp = false;
    }
    public void SetBridgyAllowed()
    {
        IsBridgyAllowed = true;
    }
    public void SetBridgyNotAllowed()
    {
        IsBridgyAllowed = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<BasicIA>()){
            IsSafeToClose = false;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<BasicIA>())
        {
            IsSafeToClose = true;
        }
    }

}
