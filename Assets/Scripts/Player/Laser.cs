
using UnityEngine;
using System.Collections.Generic;
using Tobii.Gaming;
using UnityEngine.UI;
using UnityEngine.VFX;

public class Laser : MonoBehaviour
{
    #region "Attributes"
    [Header("Laser directional parameters")]
    Camera cam;
    public float LaserForce;
    public LayerMask LayerPlaneLaser;
    public LayerMask LayerHittableThings;
    public Transform LaserOrigin;
    public GameObject GazePoint;
    public GameObject DestructionPoint;
    public GameObject PlaneEyetracker;
    public GameObject PlayerHead;
    public Animator CanvasAnimator;
    public Transform EndLaser;
    private Vector2 _lastOrientationGamepad = new Vector2(100,0);

    private bool _isEyesClosed;
    private bool _isUvMode = false;
    private bool _isLaserActive = false;
    private bool _isUVActive = false;
    private float _baseLenghtMeshLaser;
    private Vector3 _vecDirector = Vector3.zero;

    [Space]
    [Header("UV")]
    public GameObject UvLightMesh;
    public Material UVMat;
    public float GrowthSpeed;
    public bool isStartingWithUV = false;
    public Color UVLightColor;
    public List<GameObject> UVVisualEffects;


    [Space]
    [Header("Laser")]
    public GameObject LaserMesh;
    public Material LaserMat;
    public Color LaserLightColor;
    public GameObject LaserCone;
    public Light SpotLight;
    public Light BodyLight;
    public List<Renderer> RobotMeshes;
    public VisualEffect LaserNoiseParticles;
    public VisualEffect HitParticles;
    public List<GameObject> LaserVisualEffects;   

    [Space]
    [Header("Laser's painting")]
    public Color PaintColor;
    public float Radius = 1;
    public float Strenght = 1;
    public float Hardness = 1;

    [Space]
    [Header("Smoothness")]
    public float SmoothTime = 0.3f;
    private Vector3 _velocity = Vector3.zero;
    private Vector3 _target = Vector3.zero;

    [Space]
    [Header("Sounds")]
    public AK.Wwise.Event UVSound;
    public AK.Wwise.Event LaserSound;
    public float HighOffset = 0.01f;
    public float LowOffset = 0.008f;

    private GameObject _focusedObject;
    private float _timeFocused;
    private Vector3 _lastLookDirection = Vector3.zero;
    private float _rtpcValueLaserVariation = 18;
    #endregion

    #region "Methods & functions"

    private void AdjustLaserMeshSize(Vector3 _target)
    {
        float _zScale = (Mathf.Abs((_target - LaserOrigin.position).magnitude))/ _baseLenghtMeshLaser;

        LaserMesh.transform.localScale = new Vector3(LaserMesh.transform.localScale.x, LaserMesh.transform.localScale.y, _zScale);

        //Scale the laser's texture
        LaserNoiseParticles.SetFloat("NoiseLength", LaserMesh.transform.localScale.z * 6);
    }

    public void SwitchLaserOn()
    {
        if (!_isLaserActive)
        {
            _isLaserActive = true;

            //Active laser's mesh
            LaserMesh.SetActive(true);                      

            //Play Sound of laser
            LaserSound.Post(this.gameObject);

            //Active particles effect and lights
            HitParticles.Play();
            foreach (GameObject item in LaserVisualEffects)
            {
                item.SetActive(true);
            }

            //Change Lights color
            BodyLight.enabled = true;
            SpotLight.enabled = true;
            BodyLight.color = LaserLightColor;
            SpotLight.color = LaserLightColor;

            //Change laser cone mat
            LaserCone.GetComponent<Renderer>().material.SetFloat("Vector1_UvLightWarmness", 0);

            //Change robot mat
            foreach (Renderer renderer in RobotMeshes)
            {
                renderer.material = LaserMat;
                renderer.material.SetFloat("Vector1_Heat",1);
            }

            //Play damaged sound
            this.GetComponent<PlayerMovement>().PlayDamagedSound();

        }
       
    }

    public void SwitchLaserOff()
    {
        if (_isLaserActive)
        {
            _isLaserActive = false;

            //Desactive laser mesh
            LaserMesh.SetActive(false);

            //Stop laser Sound
            LaserSound.Stop(this.gameObject);
            
            //Stop particles effect
            HitParticles.Stop();
            foreach (GameObject item in LaserVisualEffects)
            {
                item.SetActive(false);
            }

            //Stop Light
            SpotLight.enabled = false;

        }
        
    }

    public void SwitchUvLightOn()
    {
        if (!_isUVActive)
        {
            _isUVActive = true;

            UvLightMesh.SetActive(true);

            //PlaySound
            UVSound.Post(this.gameObject);

            //Change Lights color         
            SpotLight.enabled = true;
            SpotLight.color = UVLightColor;

            //Active vfx uv
            foreach (GameObject visuelEffect in UVVisualEffects)
            {
                visuelEffect.SetActive(true);
            }

            //Change uv cone mat
            UvLightMesh.GetComponent<Renderer>().material.SetFloat("Vector1_UvLightWarmness", 1);

            //Change robot mat
            foreach (Renderer renderer in RobotMeshes)
            {
                renderer.material = UVMat;
            }

            //Play non damaged sound
            this.GetComponent<PlayerMovement>().PlayNonDamagedSound();

        }     
    }

    public void SwitchUvLightOff()
    {
        if (_isUVActive)
        {
            _isUVActive = false;

            UvLightMesh.SetActive(false);

            //Stop Sound 
            UVSound.Stop(this.gameObject);

            //Stop Light
            SpotLight.enabled = false;

            //Stop vfx uv
            foreach (GameObject visuelEffect in UVVisualEffects)
            {
                visuelEffect.SetActive(false);
            }

        }
        
    }

    public void SwitchModeLight()
    {
        if (_isUvMode)
        {
            SwitchUvLightOff();
            SwitchLaserOn();
            _isUvMode = false;
        }
        else
        {
            SwitchLaserOff();
            SwitchUvLightOn();
            _isUvMode = true;
        }
    }

    public void CloseEyes(bool p_value)
    {
        _isEyesClosed = p_value;

        //If player close eyes
        if(_isEyesClosed)
        {
            if (_isUvMode)
            {
                SwitchUvLightOff();
            }
            else
            {
                SwitchLaserOff();
            }
        }
        else
        {
            if (_isUvMode)
            {
                SwitchUvLightOn();
            }
            else
            {
                SwitchLaserOn();
            }
        }
    }

    public Vector3 GetLookDirection()
    {
        Ray ray;

        //if the gazePoint is not recent, user is closing his eyes
        if (InputManager.instance.IsCloseEyesAction())
        {
            CloseEyes(true);
        }
        else
        {
            CloseEyes(false);
        }

        //Eyetracker controls
        if (TobiiAPI.IsConnected && TobiiAPI.GetUserPresence().IsUserPresent())
        {
            //Get the looking point of the viewer
            GazePoint gazePoint = TobiiAPI.GetGazePoint();

            ray = cam.ScreenPointToRay(new Vector3(gazePoint.Screen.x, gazePoint.Screen.y, 0));
        }
        //Controller controls
        else
        {
            //Cursor movement
            if (!InputManager.instance.IsUsingGamepad())
            {
                float _xCursorPos = Mathf.Clamp(InputManager.instance.Cursor.transform.position.x + InputManager.instance.GetInputAimVector().x * InputManager.instance.CursorSpeedController * Time.deltaTime, 0, Screen.width);
                float _yCursorPos = Mathf.Clamp(InputManager.instance.Cursor.transform.position.y + InputManager.instance.GetInputAimVector().y * InputManager.instance.CursorSpeedController * Time.deltaTime, 0, Screen.height);

                InputManager.instance.Cursor.transform.position = new Vector3(_xCursorPos, _yCursorPos, 0);

                ray = cam.ScreenPointToRay(new Vector3(InputManager.instance.Cursor.transform.position.x, InputManager.instance.Cursor.transform.position.y, 0));
            }
            else
            {
                float _xCursorPos;
                float _yCursorPos;

                if ((InputManager.instance.GetInputAimVector().x * 100f < 50f && InputManager.instance.GetInputAimVector().x * 100f > -50f) 
                    && (InputManager.instance.GetInputAimVector().y * 100f < 50f && InputManager.instance.GetInputAimVector().y * 100f > -50f))
                {
                    _xCursorPos = Screen.width / 2.0f + (_lastOrientationGamepad.x);
                    _yCursorPos = Screen.height / 2.0f + (_lastOrientationGamepad.y);
                }
                else
                {
                    _xCursorPos = Screen.width / 2.0f + (InputManager.instance.GetInputAimVector().x * 100f);
                    _yCursorPos = Screen.height / 2.0f + (InputManager.instance.GetInputAimVector().y * 100f);

                    _lastOrientationGamepad.x = InputManager.instance.GetInputAimVector().x * 100f;
                    _lastOrientationGamepad.y = InputManager.instance.GetInputAimVector().y * 100f;
                }

                InputManager.instance.Cursor.transform.position = new Vector3(_xCursorPos, _yCursorPos, 0);

                ray = cam.ScreenPointToRay(new Vector3(InputManager.instance.Cursor.transform.position.x, InputManager.instance.Cursor.transform.position.y, 0));

                
            }
            
        }

        //Update animation of closing eyes
        CanvasAnimator.SetBool("isEyesClosed", _isEyesClosed);

        RaycastHit hit;
        // Does the ray intersect any objects in the laser layer
        if (Physics.Raycast(ray.origin, ray.direction, out hit, Mathf.Infinity, LayerPlaneLaser))
        {
            _target = new Vector3(hit.point.x, LaserOrigin.position.y, hit.point.z);

            GazePoint.transform.position = Vector3.SmoothDamp(GazePoint.transform.position, _target, ref _velocity, SmoothTime);

            //Scale the laser mesh
            Vector3 vecDirector = GazePoint.transform.position - PlayerHead.transform.position;
            vecDirector.y = 0;
            vecDirector.Normalize();
                        
            //Align the head of the robot
            PlayerHead.transform.forward = vecDirector;

            return vecDirector;
        }

        return new Vector3(0,0,0);

        
    }

    public void MoveLaser()
    {
        _lastLookDirection = _vecDirector;
        _vecDirector = GetLookDirection();

        RaycastHit hit;

        if (Physics.Raycast(LaserOrigin.position, _vecDirector, out hit, Mathf.Infinity, LayerHittableThings) && !_isEyesClosed)
        {
            DestructionPoint.transform.position = hit.point;
            AdjustLaserMeshSize(DestructionPoint.transform.position);

            if (!HitParticles.isActiveAndEnabled)
            {
                HitParticles.Play();
            }

            //Update RTPC sound focus object
            if(hit.collider.gameObject == _focusedObject)
            {
                _timeFocused += Time.deltaTime;
            }
            else
            {
                _timeFocused = 0;
                _timeFocused += Time.deltaTime;
                _focusedObject = hit.collider.gameObject;
            }
            AkSoundEngine.SetRTPCValue("GP_Laser_Time_Focus", Mathf.Min(5,_timeFocused), this.gameObject);

            //Update RTPC sound variation laser
            float offset = (Mathf.Acos(Vector3.Dot(_vecDirector, _lastLookDirection) / (_lastLookDirection.magnitude * _vecDirector.magnitude)));
            
            if (offset > HighOffset)
            {
                _rtpcValueLaserVariation = 75f;
            }
            else
            {
                if (offset > LowOffset)
                {
                    _rtpcValueLaserVariation = 72.5f;
                }
                else
                {
                    _rtpcValueLaserVariation = 65f;
                }

            }
           
            AkSoundEngine.SetRTPCValue("GP_Laser_Movement",_rtpcValueLaserVariation, this.gameObject);
            

            //Damage props
            if (hit.collider.gameObject.GetComponent<InteractableObject>())
                hit.collider.gameObject.GetComponent<InteractableObject>().GetHurt();

            //Damage robots
            if (hit.collider.gameObject.GetComponent<BasicIA>())
                hit.collider.gameObject.GetComponent<BasicIA>().Touched();

            //Pulse hit object
            if (hit.collider.gameObject.GetComponent<Rigidbody>() && !hit.collider.gameObject.GetComponent<MeltableObject>())
            {
                hit.collider.gameObject.GetComponent<Rigidbody>().AddForce(_vecDirector * LaserForce * Time.deltaTime);
            }

            //Draw laser's trail on object
            Paintable p = hit.collider.GetComponent<Paintable>();
            if (p != null)
            {
                PaintManager.instance.paint(p, hit.point, Radius, Hardness, Strenght, PaintColor);
            }

        }
        else
        {
            AdjustLaserMeshSize(LaserOrigin.position + _vecDirector * 100);
        }

    }

    public void MoveUvLight()
    {
        Vector3 vecDirector = GetLookDirection();

    }

    #endregion

    #region "Events"
    void Start()
    {
        

        if (isStartingWithUV)
        {
            //Set laser mode at the begining 
            _isLaserActive = true;
            _isUVActive = false;
            SwitchLaserOff();
            SwitchUvLightOn();
            
            _isUvMode = true;
        }
        else
        {
            //Set laser mode at the begining    
            _isLaserActive = false;
            _isUVActive = true;
            SwitchUvLightOff();
            SwitchLaserOn();
            
            _isUvMode = false;
        }
        

        cam = Camera.main;
        _baseLenghtMeshLaser = (EndLaser.position - LaserMesh.transform.position).magnitude;
    }

    void Update()
    {      
        //Set the height of the plane eyetracker to fit the height of robot's eyes
        PlaneEyetracker.transform.position = new Vector3(PlaneEyetracker.transform.position.x, LaserOrigin.transform.position.y, PlaneEyetracker.transform.position.z);

        if (InputManager.instance.IsInputEnabled())
        {
            //Update laser if it is active or if eyes ar closed
            if (_isLaserActive || _isEyesClosed)
            {
                MoveLaser();
            }

            if (_isUvMode || _isEyesClosed)
            {
                MoveUvLight();
            }
        }
        
    }
    
    #endregion
}