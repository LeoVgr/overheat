using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AK.Wwise;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    #region "Attributs"
    [Header("Movement")]
    public float MoveSpeed = 5f;
    public float MaxTiltAngle = 15;

    private Rigidbody _rigidbody;

    private Vector3 _forward;
    private Vector3 _right;
    
    private Vector2 _movement = Vector2.zero;
    private Vector3 _rightMovement = Vector3.zero;
    private Vector3 _upMovement = Vector3.zero;

    [Space]
    [Header("Sounds")]
    public AK.Wwise.Event RobotMovementSound;
    public AK.Wwise.Switch SwDamageSound;
    public AK.Wwise.Switch SwNonDamageSound;
    private Vector3 _lastPos = Vector3.zero;

    public AK.Wwise.Switch SwWoodHitSound;
    public AK.Wwise.Switch SwMetalHitSound;
    #endregion

    #region "Events"
    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }
    void Start()
    {
        //Set the drag
        _rigidbody.drag = 3;

        //Set forward and right vector of the camera to adapt movement to camera
        _forward = Camera.main.transform.forward;
        _forward.y = 0;
        _forward = Vector3.Normalize(_forward);
        _right = Vector3.Normalize(Quaternion.Euler(new Vector3(0, 90, 0)) * _forward);

        //Play movement sound
        RobotMovementSound.Post(this.gameObject);
    }
    private void FixedUpdate()
    {
        if (InputManager.instance.IsInputEnabled())
        {        
            Move();
            UpdateMovementTilt();
            UpdateMovementSound();
        }
    }
    #endregion

    #region "Methods"
    public void PlayDamagedSound()
    {
        SwDamageSound.SetValue(this.gameObject);
    }
    public void PlayNonDamagedSound()
    {
        SwNonDamageSound.SetValue(this.gameObject);
    }
    private void Move()
    {
        //get inputs
        _movement = InputManager.instance.GetInputMovementVector();

        //Compute movement vector
        _rightMovement = _right * _movement.x;
        _upMovement = _forward * _movement.y;

        //Set the next position of the rigidbody
        _rigidbody.velocity = (_rightMovement + _upMovement) * MoveSpeed * Time.fixedDeltaTime;

    }
    private void UpdateMovementSound()
    {
        AkSoundEngine.SetRTPCValue("GP_MainRobot_Movement", (_rigidbody.velocity.magnitude/6f)*50f, this.gameObject);
        _lastPos = this.gameObject.transform.position;
    }
    private void UpdateMovementTilt()
    {
        this.gameObject.transform.rotation = Quaternion.AngleAxis(MaxTiltAngle * _movement.y, _right) * Quaternion.AngleAxis(MaxTiltAngle * _movement.x, -_forward);
    }

    #endregion

}
