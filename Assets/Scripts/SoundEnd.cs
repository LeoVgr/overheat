using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEnd : MonoBehaviour
{
    public MecanicianManager Manager;
    public AK.Wwise.Event ArmBackwardSound;
    public AK.Wwise.Event ArmMetalImpactSound;
    public AK.Wwise.Event ArmMovement2Sound;
    public AK.Wwise.Event ArmUpSound;
    public AK.Wwise.Event FingerClapSound;
    public AK.Wwise.Event Hand1Sound;
    public AK.Wwise.Event HeadMovement2and3Sound;
    public AK.Wwise.Event HeadMovementLittleSound;
    public AK.Wwise.Event MetalSmallImpactSound;
    public AK.Wwise.Event DestructionSound;
    public AK.Wwise.Event RepairSound;

    public void PlayArmBackwardSound()
    {
        ArmBackwardSound.Post(this.gameObject);
    }
    public void PlayArmMetalImpactSound()
    {
        ArmMetalImpactSound.Post(this.gameObject);
    }
    public void PlayArmMovement2Sound()
    {
        ArmMovement2Sound.Post(this.gameObject);
    }
    public void PlayArmUpSound()
    {
        ArmUpSound.Post(this.gameObject);
    }
    public void PlayFingerClapSound()
    {
        FingerClapSound.Post(this.gameObject);
    }
    public void PlayHand1Sound()
    {
        Hand1Sound.Post(this.gameObject);
    }
    public void PlayHeadMovement2and3Sound()
    {
        HeadMovement2and3Sound.Post(this.gameObject);
    }
    public void PlayHeadMovementLittleSound()
    {
        HeadMovementLittleSound.Post(this.gameObject);
    }
    public void PlayMetalSmallImpactSound()
    {
        MetalSmallImpactSound.Post(this.gameObject);
    }
    public void PlayEndSound()
    {
        if (FlammableObject.DestroyedProps >= Manager.LimitPropsNumber)
        {
            DestructionSound.Post(this.gameObject);
        }
        else
        {
            RepairSound.Post(this.gameObject);
        }
    }
}
