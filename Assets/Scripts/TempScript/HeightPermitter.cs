using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeightPermitter : MonoBehaviour
{
    public GameObject playerBody;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == playerBody)
        {
            Rigidbody rb = playerBody.GetComponent<Rigidbody>();
            Debug.LogError("DLAMERDE");
            if ((rb.constraints & RigidbodyConstraints.FreezePositionY) == RigidbodyConstraints.FreezePositionY)
            {
                rb.constraints = RigidbodyConstraints.None;
                rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY |RigidbodyConstraints.FreezeRotationZ;
            }
            else
                rb.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
        }

    }
}
