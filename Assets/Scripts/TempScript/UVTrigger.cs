using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UVTrigger : MonoBehaviour
{
    [SerializeField] private GameObject player;
    private void OnTriggerStay(Collider other)
    {
        if (other.GetComponent<Growable>())
        {
            other.GetComponent<Growable>().Grow(player.GetComponent<Laser>().GrowthSpeed * Time.deltaTime);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Growable>())
        {
            other.GetComponent<Growable>().StopGrow();
        }
    }
}
