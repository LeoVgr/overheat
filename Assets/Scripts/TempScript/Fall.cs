using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class Fall : MonoBehaviour
{
    public GameObject Player;
    public AK.Wwise.Event AlarmSound;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == Player)
        {
            //Prepare the cutscene
            GameManager.instance.StartCutScene();
            GameManager.instance.PlayAccidentCutScene();
            this.gameObject.SetActive(false);
            Invoke("SwitchLight", (float)GameManager.instance.PlayableDirector.duration);
            
        }

    }

    private void SwitchLight()
    {
        //End the cutscene
        GameManager.instance.EndCutScene();
        AlarmSound.Post(Player);
        Player.GetComponent<Laser>().SwitchModeLight();      
    }

}
