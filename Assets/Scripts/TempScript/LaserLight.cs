using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserLight : MonoBehaviour
{
    public GameObject player;
    public Light light1;
    public Light light2;
    public Light light3;
    public Light light4;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            Camera.main.GetComponent<Laser>().SwitchModeLight();
        }

    }
}
