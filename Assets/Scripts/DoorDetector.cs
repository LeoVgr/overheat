using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorDetector : MonoBehaviour
{
    public Animator DoorAnimator;
    public AK.Wwise.Event DoorSound;

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Laser>())
        {
            DoorAnimator.SetTrigger("Open");
            DoorSound.Post(DoorAnimator.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Laser>())
        {
            DoorAnimator.SetTrigger("Close");
            DoorSound.Post(DoorAnimator.gameObject);
        }
    }
}
