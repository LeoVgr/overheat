using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class DestroyableRobot : MonoBehaviour
{
    public List<GameObject> DisassamblePart;
    public float Force;
    public float MaxVelocity;
    public AK.Wwise.Event StartBrokenSound;
    public AK.Wwise.Event EndBrokenSound;

    private bool _isDisassambled = false;

    private void Update()
    {
        if (_isDisassambled)
        {       
            if(this.gameObject.GetComponent<Rigidbody>().velocity.magnitude < MaxVelocity)
                this.GetComponent<Rigidbody>().AddForceAtPosition(new Vector3(this.transform.forward.x, 0, this.transform.forward.z) * Force, DisassamblePart[0].transform.position);
        }
    }
    public void Disassamble()
    {
        StartBrokenSound.Post(this.gameObject);
        _isDisassambled = true; 

        //Remove One part of the robot
        GameObject part = DisassamblePart[Random.Range(0, DisassamblePart.Count)];
        DisassamblePart.Remove(part);
        part.AddComponent<Rigidbody>();
        part.transform.parent = null;

        //Add rigibody
        this.gameObject.AddComponent<Rigidbody>();
        this.gameObject.GetComponent<Rigidbody>().mass = 5;
        this.gameObject.GetComponent<Rigidbody>().isKinematic = false;
        this.GetComponent<Rigidbody>().maxAngularVelocity = 60 * (Mathf.PI / 180f);

        //Remove IA
        this.gameObject.GetComponent<NavMeshAgent>().enabled = false;

        //Remove attache to props 
        this.gameObject.GetComponent<WearerIA>().WearingPlatform.SetActive(false);
        this.gameObject.GetComponent<WearerIA>().enabled = false;

    }
   
}
