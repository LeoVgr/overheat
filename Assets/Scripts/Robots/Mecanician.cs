using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mecanician : MonoBehaviour
{
    public AK.Wwise.Event ArmBack;
    public AK.Wwise.Event ArmBackPhase2;
    public AK.Wwise.Event ArmDown;
    public AK.Wwise.Event HandPick;
    public AK.Wwise.Event HandPickAll;
    public AK.Wwise.Event Turn;
    public AK.Wwise.Event HeadLittleZoom;
    public AK.Wwise.Event HeadZoom;

    public void PlayArmBackSound()
    {
        ArmBack.Post(this.gameObject);
    }
    public void PlayArmBackPhase2Sound()
    {
        ArmBackPhase2.Post(this.gameObject);
    }
    public void PlayArmDownSound()
    {
        ArmDown.Post(this.gameObject);
    }
    public void PlayHandPickSound()
    {
        HandPick.Post(this.gameObject);
    }
    public void PlayHandPickAllSound()
    {
        HandPickAll.Post(this.gameObject);
    }
    public void PlayTurnSound()
    {
        Turn.Post(this.gameObject);
    }
    public void PlayHeadLittleZoomSound()
    {
        HeadLittleZoom.Post(this.gameObject);
    }
    public void PlayHeadZoomSound()
    {
        HeadZoom.Post(this.gameObject);
    }
}
