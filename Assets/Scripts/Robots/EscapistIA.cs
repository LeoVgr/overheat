using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscapistIA : BasicIA
{
    #region "Attributs"
    public GameObject Player;
    public float RadiusHide = 10f;
    public float TimeHide = 5f;
    public LayerMask HiddingLayer;

    private float _timerWork = 0;
    private float _totalHiddingTime;
    private Vector3 _targetHidden = Vector3.zero;
    private Vector3 _centerPlayerPosition = Vector3.zero;

    #endregion

    #region "Methods"
    public override void Touched()
    {       
        if (!_isTouched)
        {
            //Boost agent speed
            Agent.speed *= 2;
            Agent.acceleration *= 2;
        }

        base.Touched();
    }
    protected override void TouchedBehavior()
    {
        //Set the player position a bit higher than the pivot
        _centerPlayerPosition = Player.transform.position + new Vector3(0, 1, 0);

        //Go to the hiding point while it is hidden from the player
        if (IsVisibleByPlayer(_targetHidden))
            _targetHidden = Hide();

        Agent.SetDestination(_targetHidden);
    }
    private Vector3 Escape()
    {
        //Escape from the player
        Vector3 oppositeDirection = this.transform.position - Player.transform.position;
        oppositeDirection.Normalize();

        return new Vector3(oppositeDirection.x + this.transform.position.x, this.transform.position.y, oppositeDirection.z + this.transform.position.z);
    }
    private Vector3 Hide()
    {
        //Reset behavior after some time
        _timerWork += Time.deltaTime;
        _totalHiddingTime += Time.deltaTime;

        if (_timerWork >= TimeHide)
            ResetTouched();

        print(_targetHidden);
        

        //If we are not hidden from the player
        if (IsVisibleByPlayer(this.transform.position))
        {
            Collider[] colliders = Physics.OverlapSphere(this.transform.position, RadiusHide);

            foreach (Collider hit in colliders)
            {
                //Skip our gameobject and player's gameobject
                if (hit.gameObject == this.gameObject || hit.gameObject == Player.gameObject)
                    continue;

                //Opposite direction from the player
                Vector3 oppositeDirection = hit.transform.position - _centerPlayerPosition;
                oppositeDirection.Normalize();

                Vector3 hiddenPos = new Vector3(hit.transform.position.x + oppositeDirection.x * 2, this.transform.position.y, hit.transform.position.z + oppositeDirection.z * 2);

                //Check if the position is visible by the player and it is on the navmesh
                if (IsVisibleByPlayer(hiddenPos) && IsOnNavmesh(hiddenPos))
                {
                    return hiddenPos;
                }
            }
        }

        

        return this.transform.position;
    }
    private bool IsVisibleByPlayer(Vector3 position)
    {
        RaycastHit rayHit;

        //Check if we are hidden at this position
        if (Physics.Raycast(this.transform.position, _centerPlayerPosition - this.transform.position, out rayHit))
        {
            //Check if the point is behind the player 
            if (rayHit.collider.gameObject!= this.gameObject && (_centerPlayerPosition - this.transform.position).magnitude > (rayHit.point - this.transform.position).magnitude)
            {
                return false;
            }
        }
        return true;
    }
    private void ResetTouched()
    {
        if (_isTouched)
        {
            _isTouched = false;

            //Reset timer
            _timerWork = 0;

            //Boost agent speed
            Agent.speed /= 2;
            Agent.acceleration /= 2;
        }
    }
    #endregion
}
