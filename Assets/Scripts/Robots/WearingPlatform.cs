using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WearingPlatform : MonoBehaviour
{
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.transform.parent != null)
            collision.gameObject.transform.parent = null;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.transform.parent != null && other.GetComponent<Rigidbody>())
            other.gameObject.transform.parent = null;
    }


}
