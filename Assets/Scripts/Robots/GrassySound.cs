using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrassySound : MonoBehaviour
{
    public AK.Wwise.Event CutSound;
    public void PlayCutSound()
    {
        CutSound.Post(this.gameObject);
    }
}
