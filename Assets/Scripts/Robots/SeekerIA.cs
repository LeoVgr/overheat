using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SeekerIA : BasicIA
{
    protected override void TouchedBehavior()
    {
        if(this.GetComponent<NavMeshAgent>().enabled)
            Agent.SetDestination(FollowWayPoints());
    }
}
