using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotActiver : MonoBehaviour
{
    private void OnTriggerStay(Collider other)
    {
        if (other.GetComponent<BasicIA>() && other.GetComponent<BasicIA>().IsStopped)
        {
            other.GetComponent<BasicIA>().ActiveRobot();
        }
    }
}
