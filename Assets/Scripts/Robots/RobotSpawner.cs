using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotSpawner : MonoBehaviour
{
    public List<GameObject> PrefabsRobot;
    public List<GameObject> WayPoints;
    public float TimeBetweenSpawns;

    private float _timer = 0;
    
    // Update is called once per frame
    void Update()
    {
        _timer += Time.deltaTime;

        if(_timer>= TimeBetweenSpawns)
        {
            _timer = 0;

            int randomNumber = Random.Range(0, PrefabsRobot.Count);
            Vector3 SpawnPos = Vector3.zero;

            if (PrefabsRobot[randomNumber].GetComponent<Renderer>())
            {
                SpawnPos = new Vector3(this.transform.position.x, this.transform.position.y + PrefabsRobot[randomNumber].GetComponent<Renderer>().bounds.size.y, this.transform.position.z);
            }
            else
            {
                SpawnPos = new Vector3(this.transform.position.x, this.transform.position.y + PrefabsRobot[randomNumber].transform.GetChild(0).GetComponent<Renderer>().bounds.size.y, this.transform.position.z);
            }
            

            GameObject robot = Instantiate(PrefabsRobot[randomNumber], SpawnPos, this.transform.rotation);
            foreach (GameObject wayPoint in WayPoints)
            {
                robot.GetComponent<BasicIA>().WayPoints.Add(wayPoint);
            }
            
        }
    }
}
