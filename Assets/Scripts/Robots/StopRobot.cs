using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StopRobot : MonoBehaviour
{
    public bool isThereARobot = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<BasicIA>())
        {
            other.gameObject.GetComponent<BasicIA>().StopRobot();
            isThereARobot = true;
        }
                      

        
    }
}
