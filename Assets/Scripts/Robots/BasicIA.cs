using UnityEngine.AI;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class BasicIA : MonoBehaviour
{
    #region "Attributs"
    [Header("IA")]
    public NavMeshAgent Agent;
    
    public List<GameObject> WayPoints;
    public float DistanceToWaypointNeeded = 0.001f;
    protected bool _isTouched = false;

    private GameObject _currentWayPoint;
    private float _timer = 0;
    private int _index = 0;
    private Transform _target;
    private float _smoothTime = 0.3F;
    private Vector3 _angularVelocity = Vector3.zero;

    [Space]
    [Header("Animation")]
    private Animator _animator;
    private bool _isWorking = false;
    private bool _isMoving = true;
    public bool IsStopped = false;

    [Space]
    [Header("Sounds")]
    public AK.Wwise.Event StartMovingSound;
    public AK.Wwise.Event StopMovingSound;
    

    #endregion

    #region "Events"
    public virtual void Start()
    {
        _animator = this.GetComponent<Animator>();

         //Initialize the first way point
         _currentWayPoint = WayPoints[0];
        Agent.SetDestination(new Vector3(_currentWayPoint.transform.position.x, this.transform.position.y, _currentWayPoint.transform.position.z));

        //Moving sound
        _isWorking = false;
        _isMoving = true;
        StartMovingSound.Post(this.gameObject);
    }
    protected void Update()
    {
        if (!IsStopped)
        {
            if (!_isTouched)
            {
                Agent.SetDestination(FollowWayPoints());
            }
            else
            {
                TouchedBehavior();
            }
        }
        else
        {
            Agent.SetDestination(this.transform.position);
        }  

    }
    #endregion

    #region "Methods"
    public virtual void Touched()
    {
        if (!_isTouched)
        {
            _isTouched = true;

            if (this.GetComponent<DestroyableRobot>())
                this.GetComponent<DestroyableRobot>().Disassamble();

        }        

    }
    protected abstract void TouchedBehavior();
    protected Vector3 FollowWayPoints()
    {     
        //Go to the way point if we are not at his point
        if (this.GetComponent<NavMeshAgent>().enabled && Agent.remainingDistance > DistanceToWaypointNeeded)
        {
            return new Vector3(_currentWayPoint.transform.position.x, this.transform.position.y, _currentWayPoint.transform.position.z);
        }
        else
        {
            //Wait on the waypoint or not
            if(_timer >= _currentWayPoint.GetComponent<Waypoint>().Time)
            {
                if (_isWorking && !_isMoving)
                {
                    _isWorking = false;
                    _isMoving = true;

                    if (_animator != null)
                    {
                        _animator?.SetBool("isWorking", _isWorking);
                        _animator?.SetBool("isMoving", _isMoving);
                        StartMovingSound.Post(this.gameObject);
                    }
                       
                }


                _timer = 0;
         
                //Go to the next way point
                if (_index == WayPoints.Count - 1)
                {
                    _index = 0;
                }
                else
                {
                    _index++;
                }
                _currentWayPoint = WayPoints[_index];
                return new Vector3(_currentWayPoint.transform.position.x, this.transform.position.y, _currentWayPoint.transform.position.z);
            }
            else
            {                
                //Align the robot to the waypoint before start the count down
                if (!Approximately(this.transform.rotation, _currentWayPoint.transform.rotation, 1E-05f))
                {
                    Approximately(this.transform.rotation, _currentWayPoint.transform.rotation, 1E-05f);
                    _target = _currentWayPoint.transform;
                    transform.rotation = SmoothDampQuaternion(this.transform.rotation, _target.rotation, ref _angularVelocity, _smoothTime);
                }
                else
                {
                    if (!_isWorking && _isMoving)
                    {
                        _isWorking = true;
                        _isMoving = false;

                        if (_animator != null)
                        {
                            _animator?.SetBool("isWorking", _isWorking);
                            _animator?.SetBool("isMoving", _isMoving);
                            StopMovingSound.Post(this.gameObject);
                        }

                    }

                    _timer += Time.deltaTime;
                }
                
                return this.transform.position;
            }
                        
        }
    }
    public bool Approximately(Quaternion quatA, Quaternion value, float acceptableRange)
    {     
        return 1 - Mathf.Abs(Quaternion.Dot(quatA, value)) < acceptableRange;
    }
    public Quaternion SmoothDampQuaternion(Quaternion current, Quaternion target, ref Vector3 currentVelocity, float smoothTime)
    {
        Vector3 c = current.eulerAngles;
        Vector3 t = target.eulerAngles;
        return Quaternion.Euler(
          Mathf.SmoothDampAngle(c.x, t.x, ref currentVelocity.x, smoothTime),
          Mathf.SmoothDampAngle(c.y, t.y, ref currentVelocity.y, smoothTime),
          Mathf.SmoothDampAngle(c.z, t.z, ref currentVelocity.z, smoothTime)
        );
    }
    protected bool IsOnNavmesh(Vector3 position)
    {
        NavMeshHit navHit;
        return NavMesh.SamplePosition(position, out navHit, 1f, NavMesh.AllAreas);
    }
    public void StopRobot()
    {
        _isWorking = false;
        _isMoving = false;
        IsStopped = true;
        StopMovingSound.Post(this.gameObject);
    }
    public void ActiveRobot()
    {
        _isWorking = false;
        _isMoving = true;
        IsStopped = false;
        StartMovingSound.Post(this.gameObject);
    }

    #endregion
}
