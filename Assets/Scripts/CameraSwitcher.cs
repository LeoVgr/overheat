using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitcher : MonoBehaviour
{
    public CinemachineBrain OrthoCam;
    public CinemachineVirtualCamera OrthoV1;
    public CinemachineVirtualCamera OrthoV2;

    public CinemachineBrain PerspCam;
    public CinemachineVirtualCamera PerspV1;
    public CinemachineVirtualCamera PerspV2;

    private bool isPersp = true;
    // Start is called before the first frame update
    void Start()
    {
        OrthoCam.gameObject.SetActive(false);
        OrthoV1.gameObject.SetActive(false);
        OrthoV2.gameObject.SetActive(false);

        PerspCam.gameObject.SetActive(true);
        PerspV1.gameObject.SetActive(true);
        PerspV2.gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.C))
        {
            isPersp = !isPersp;
            OrthoCam.gameObject.SetActive(!isPersp);
            OrthoV1.gameObject.SetActive(!isPersp);
            OrthoV2.gameObject.SetActive(!isPersp);

            PerspCam.gameObject.SetActive(isPersp);
            PerspV1.gameObject.SetActive(isPersp);
            PerspV2.gameObject.SetActive(isPersp);
        }
    }
}
