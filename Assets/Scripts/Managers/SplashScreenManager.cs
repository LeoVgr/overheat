using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class SplashScreenManager : MonoBehaviour
{
    public List<Animator> SplashScreenAnimator;
    public float DisplayTime = 3f;
    public float TransitionTime = 1f;
    public float PauseTime = 0.5f;

    private bool _isLoaded = true;
    private int _splashScreenIndex = 0;
    private float _timer = 0;

    private void Start()
    {
        _timer = DisplayTime;
    }
    private void Update()
    {

        if (_isLoaded)
            _timer += Time.deltaTime;

        if(_timer >= DisplayTime)
        {
            StartCoroutine("LoadNextSplashScreen");
        }       
    }
    IEnumerator LoadNextSplashScreen()
    {
        _isLoaded = false;
        _timer = 0;

        //First Splash Screen
        if (_splashScreenIndex == 0)
        {
            SplashScreenAnimator[_splashScreenIndex].SetTrigger("Start");

            yield return new WaitForSeconds(TransitionTime);

            _isLoaded = true;
        }

        //Between SplashScreen
        if (_splashScreenIndex != 0 && _splashScreenIndex != SplashScreenAnimator.Count)
        {
            SplashScreenAnimator[_splashScreenIndex - 1].SetTrigger("End");
            yield return new WaitForSeconds(TransitionTime + PauseTime);
            SplashScreenAnimator[_splashScreenIndex].SetTrigger("Start");
            
            yield return new WaitForSeconds(TransitionTime);

            _isLoaded = true;
        }

        //Last SplashScreen
        if (_splashScreenIndex == SplashScreenAnimator.Count)
        {
            SplashScreenAnimator[_splashScreenIndex - 1].SetTrigger("End");

            yield return new WaitForSeconds(TransitionTime);

            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }

        ++_splashScreenIndex;        
    }

}
