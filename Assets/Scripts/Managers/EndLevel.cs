using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndLevel : MonoBehaviour
{
    public AK.Wwise.Event StopAllSound;

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Laser>())
        {
            StopAllSound.Post(this.gameObject);
            LevelLoader.instance.LoadNextLevel();
        }
    }
}
