using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
    #region "Attributs"
    [Space]
    [Header("Camera")]
    public CinemachineDollyCart Cart;
    public CinemachineVirtualCamera VirtualMainCamera;
    public CinemachineVirtualCamera VirtualCameraTrack;
    public float TransitionTimeEndCameraTrack = 1f;

    private bool _isInCameraTrack = false;
    private bool _isCameraTrackEnded = false;

    [Space]
    [Header("CutScenes")]
    public Animator BlackBandsAnimator;
    public Animator Instructions;
    public PlayableDirector PlayableDirector;
    public PlayableAsset TutorialCutScene;
    public PlayableAsset AccidentCutScene;
    public float TimeStartTutoCutScene = 1f;
    public bool PlayCutScene;
    #endregion

    #region "Events"
    private void Start()
    {
        if (PlayCutScene)
        {
            //Always Start with a track of the level
            StartLevelTrack();
        }       
    }
    private void Update()
    {
        Cursor.lockState = CursorLockMode.Confined; // keep confined in the game window
        Cursor.visible = false;

        //Stop the camera track when the cart reach the end of the path
        if (PlayCutScene && !_isCameraTrackEnded && Cart.m_Position >= Cart.m_Path.PathLength - 0.1f)
        {
            _isCameraTrackEnded = true;
            StartCoroutine(EndLevelTrack());
        }

        Debug.LogError(FlammableObject.DestroyedProps);
    }
    #endregion

    #region "Methods"
    public void StartCutScene()
    {
        InputManager.instance.DisableInputs();
        BlackBandsAnimator.SetTrigger("StartCutScene");
    }
    public void EndCutScene()
    {
        InputManager.instance.EnableInputs();
        BlackBandsAnimator.SetTrigger("EndCutScene");
    }
    public void SwitchCamera()
    {
        if (_isInCameraTrack)
        {
            VirtualCameraTrack.Priority = VirtualMainCamera.Priority - 1;
            _isInCameraTrack = false;
        }
        else
        {
            VirtualCameraTrack.Priority = VirtualMainCamera.Priority + 1;
            _isInCameraTrack = true;
        }
    }
    public void StartLevelTrack()
    {
        //Prepare cutscene
        StartCutScene();

        //Set the camera to camera track
        SwitchCamera();

        //Show Instructions
        Instructions.SetTrigger("Open");
    }
    public IEnumerator EndLevelTrack()
    {
        yield return new WaitForSeconds(TransitionTimeEndCameraTrack);

        //Unshow
        Instructions.SetTrigger("Close");

        //Set the camera to camera track
        SwitchCamera();

        
        //Start tutorial Cutscene if we are in tutorial       
        if (SceneManager.GetActiveScene().buildIndex == 2) // Change if we add menu scene
        {
            StartCoroutine(PlayTutorialCutScene());
        }
        else
        {
            EndCutScene();
        }
    }
    public IEnumerator PlayTutorialCutScene()
    {
        yield return new WaitForSeconds(TimeStartTutoCutScene);

        PlayableDirector.playableAsset = TutorialCutScene;
        PlayableDirector.extrapolationMode = DirectorWrapMode.None;
        PlayableDirector.Play();

        yield return new WaitForSeconds((float)TutorialCutScene.duration);

        EndCutScene();
    }
    public void PlayAccidentCutScene()
    {
        PlayableDirector.extrapolationMode = DirectorWrapMode.None;
        PlayableDirector.playableAsset = AccidentCutScene;

        PlayableDirector.Play();
    }
    #endregion

}
