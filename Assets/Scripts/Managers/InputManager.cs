using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Tobii.Gaming;

public class InputManager : Singleton<InputManager>
{
    #region "Attributs"
    [Space]
    [Header("Joystick controls")]
    public GameObject Cursor;
    public float CursorSpeedController;

    private PlayerControls _controls;
    private bool _isUsingEyeTracker = false;
    private bool _isUsingGamepad = false;
    private bool _isUsingKeyboardMouse = false;
    private bool _isInputEnabled = true;

    /* Inputs */
    private Vector2 _inputMovementVector = Vector2.zero;
    private Vector2 _inputAimVector = Vector2.zero;
    private bool _isInteractAction = false;
    private bool _isCancelAction = false;
    private bool _isCloseEyesAction = false;
    #endregion

    #region "Events"
    public override void Awake()
    {
        base.Awake();
        
        Cursor = GameObject.Find("Cursor");

        //Enable cursor by default 
        Cursor?.SetActive(true);

        UpdateDevicesUse();
    }
    private void Start()
    {
        //Enables controls
        _controls = new PlayerControls();
        _controls.Enable();        

        //Call events on input pressing
        _controls.Gameplay.Move.performed += UpdateInputMovementVector;
        _controls.Gameplay.Move.canceled += UpdateInputMovementVector;

        _controls.Gameplay.Aim.performed += UpdateInputAimVector;
        _controls.Gameplay.Aim.canceled += UpdateInputAimVector;

        _controls.Gameplay.CloseEyes.performed += UpdateCloseEyes;
        _controls.Gameplay.Interact.performed += UpdateInteract;
        _controls.Gameplay.Cancel.performed += UpdateCancel;
        _controls.Gameplay.CloseEyes.canceled += UpdateCloseEyes;
        _controls.Gameplay.Interact.canceled += UpdateInteract;
        _controls.Gameplay.Cancel.canceled += UpdateCancel;
    }
    private void Update()
    {
        UpdateDevicesUse();
    }
    private void OnApplicationQuit()
    {
        Gamepad.current?.ResetHaptics();
    }
    #endregion

    #region "Methods"
    public void DisableInputs()
    {
        _isInputEnabled = false;

        if(Cursor == null)
            Cursor = GameObject.Find("Cursor");

        if (!_isUsingEyeTracker)
        {
            Cursor?.SetActive(false);
        }
           
    }
    public void EnableInputs()
    {
        _isInputEnabled = true;

        if (!_isUsingEyeTracker)
        {
            Cursor?.SetActive(true);
        }          
    }
    public bool IsInputEnabled()
    {
        return _isInputEnabled;
    }

    public void UpdateInputMovementVector(InputAction.CallbackContext context)
    {
        _inputMovementVector = context.ReadValue<Vector2>();      
    }
    public void UpdateInputAimVector(InputAction.CallbackContext context)
    {
        _inputAimVector = context.ReadValue<Vector2>();
    }
    public void UpdateCloseEyes(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            _isCloseEyesAction = true;
        }

        if (context.canceled)
        {
            _isCloseEyesAction = false;
        }
        
    }
    public void UpdateInteract(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            _isInteractAction = true;
        }

        if (context.canceled)
        {
            _isInteractAction = false;
        }
    }
    public void UpdateCancel(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            _isCancelAction = true;
        }

        if (context.canceled)
        {
            _isCancelAction = false;
        }
    }

    public Vector2 GetInputMovementVector()
    {
        return _inputMovementVector;
    }
    public Vector2 GetInputAimVector()
    {
        return _inputAimVector;
    }
    public bool IsInteractAction()
    {
        return _isInteractAction;
    }
    public bool IsCancelAction()
    {
        return _isCancelAction;
    }
    public bool IsCloseEyesAction()
    {
        if (_isUsingEyeTracker && TobiiAPI.GetUserPresence().IsUserPresent() && !TobiiAPI.GetGazePoint().IsRecent()) 
        {
            return true;
        }
        else
        {
            return _isCloseEyesAction;
        }      
    }
    public void UpdateDevicesUse()
    {
        //Switch between devices 
        if (TobiiAPI.IsConnected)
        {
            _isUsingEyeTracker = true;
            Cursor?.SetActive(false);
        }

        if (Gamepad.current != null)
        {
            _isUsingGamepad = true;
            Cursor?.SetActive(false);
        }

        if (Keyboard.current != null && Mouse.current != null)
        {
            _isUsingKeyboardMouse = true;
        }

        CursorSpeedController = PlayerPrefs.GetFloat("Sensibility", 100);
    }
    public void AddControllerVibrations(float duration, float intensity)
    {
        Gamepad.current.SetMotorSpeeds(1-intensity,intensity);
        StartCoroutine(ResetControllerVibrations(duration));
    }
    IEnumerator ResetControllerVibrations(float duration)
    {
        if(duration > 0)
        {
            yield return new WaitForSeconds(.1f);
            duration-=0.1f;
        }
        Gamepad.current.SetMotorSpeeds(0, 0);
    }

    public bool IsUsingEyeTracker()
    {
        return (_isUsingEyeTracker);
    }
    public bool IsUsingGamepad()
    {
        return (_isUsingGamepad);
    }
    public bool IsUsingKeyboard()
    {
        return _isUsingKeyboardMouse;
    }
    #endregion
}
