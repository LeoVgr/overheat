using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;

public class MecanicianManager : MonoBehaviour
{
    public PlayableAsset End;
    public int LimitPropsNumber;
    public AK.Wwise.Event StopAllSound;


    private void Start()
    {
        this.GetComponent<PlayableDirector>().playableAsset = End;
        this.GetComponent<PlayableDirector>().Play();
        StartCoroutine("BackToMainMenu", End.duration);
    }
    IEnumerator BackToMainMenu(float duration)
    {
        yield return new WaitForSeconds(duration);
        StopAllSound.Post(this.gameObject);
        SceneManager.LoadScene(1);
    }

    
}
