using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : Singleton<LevelLoader>
{
    public Animator Transition;
    public float TransitionTime = 1f;

    public void LoadNextLevel()
    {
        StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + 1));
    }

    IEnumerator LoadLevel(int levelIndex)
    {
        Transition.SetTrigger("Start");
        
        yield return new WaitForSeconds(TransitionTime);
        
        SceneManager.LoadScene(levelIndex);
        
    }
}
