using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public EventSystem EvtSystem;
    public GameObject SplashScreen;
    public GameObject MainMenu;
    public GameObject GamepadBarIndications;
    public GameObject KeyboardIndications;

    public Animator AnimationMainMenu;
    public Animator Fade;

    public List<Button> ButtonListMainMenu;
    public Slider GeneralVolumeSlider;
    public Slider SensibilitySlider;
    public float generalVolume, sensibility;

    public AK.Wwise.Event SelectedSound;
    public AK.Wwise.Event ConfirmSound;

    private bool _isMainMenuActive = false;
    private bool _isCreditActive = false;
    private bool _isOptionActive = false;

    private void Start()
    {
        Cursor.visible = true;

        if(!InputManager.instance.IsUsingGamepad())
            EvtSystem.firstSelectedGameObject = null;

        GamepadBarIndications.SetActive(InputManager.instance.IsUsingGamepad());
        KeyboardIndications.SetActive(!InputManager.instance.IsUsingGamepad());

        SplashScreen.SetActive(true);
        MainMenu.SetActive(false);

        generalVolume = PlayerPrefs.GetFloat("GeneralV", 100);
        AkSoundEngine.SetRTPCValue("GP_Volume_Master_Sliders", generalVolume);
        GeneralVolumeSlider.SetValueWithoutNotify(generalVolume / 10);

        sensibility = PlayerPrefs.GetFloat("Sensibility", 100);
        SensibilitySlider.SetValueWithoutNotify(sensibility / 10);
    }
    private void Update()
    {
        if(!_isMainMenuActive && Input.anyKey)
        {
            _isMainMenuActive = true;
            ConfirmSound.Post(this.gameObject);
            AnimationMainMenu.SetTrigger("Start");
        }
        
        if(_isCreditActive && InputManager.instance.IsCancelAction())
        {
            CloseCredits();
        }

        if (_isOptionActive && InputManager.instance.IsCancelAction())
        {
            CloseOptions();
        }
    }
    public void NewGame()
    {
        ConfirmSound.Post(this.gameObject);
        StartCoroutine("PlayNewGame");
    }
    public void OpenOptions()
    {
        if (!_isOptionActive)
        {
            DisableMainMenuButton();
            _isOptionActive = true;
            ConfirmSound.Post(this.gameObject);
            AnimationMainMenu.SetTrigger("OpenOptions");

            if(InputManager.instance.IsUsingGamepad())
                EvtSystem.SetSelectedGameObject(GeneralVolumeSlider.gameObject);

            MainMenu.GetComponent<Image>().raycastTarget = false;
        }
    }
    public void CloseOptions()
    {
        if (_isOptionActive)
        {
            EnableMainMenuButton();
            _isOptionActive = false;
            ConfirmSound.Post(this.gameObject);
            AnimationMainMenu.SetTrigger("CloseOptions");

            if (InputManager.instance.IsUsingGamepad())
                EvtSystem.SetSelectedGameObject(ButtonListMainMenu[1].gameObject);

            MainMenu.GetComponent<Image>().raycastTarget = true;
        }
    }
    public void OpenCredits()
    {
        if (!_isCreditActive)
        {
            DisableMainMenuButton();
            _isCreditActive = true;
            ConfirmSound.Post(this.gameObject);
            AnimationMainMenu.SetTrigger("OpenCredit");
        }
           
    }
    public void CloseCredits()
    {
        if (_isCreditActive)
        {
            EnableMainMenuButton();
            _isCreditActive = false;
            ConfirmSound.Post(this.gameObject);
            AnimationMainMenu.SetTrigger("CloseCredit");
        }
    }
    public void Quit()
    {
        ConfirmSound.Post(this.gameObject);
        Application.Quit();
    }
    public void PlayHooverSound()
    {
        SelectedSound.Post(this.gameObject);
    }
    IEnumerator PlayNewGame()
    {
        Fade.SetTrigger("Play");

        yield return new WaitForSeconds(1f);

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    public void UpdateGeneral()
    {
        generalVolume = GeneralVolumeSlider.value * 10;
        PlayerPrefs.SetFloat("GeneralV", generalVolume);
        AkSoundEngine.SetRTPCValue("GP_Volume_Master_Sliders", generalVolume);
        SelectedSound.Post(this.gameObject);
    }
    public void UpdateSensibility()
    {
        sensibility = SensibilitySlider.value * 10;
        PlayerPrefs.SetFloat("Sensibility", sensibility);
        AkSoundEngine.SetRTPCValue("GP_Volume_Slider_Musique", sensibility);
        SelectedSound.Post(this.gameObject);
    }
    public void DisableMainMenuButton()
    {
        foreach (Button button in ButtonListMainMenu)
        {
            Navigation nav = new Navigation();
            nav.mode = Navigation.Mode.None;
;           button.navigation = nav;
        }
    }
    public void EnableMainMenuButton()
    {
        foreach (Button button in ButtonListMainMenu)
        {
            Navigation nav = new Navigation();
            nav.mode = Navigation.Mode.Vertical;
            button.navigation = nav;
        }
    }
}
