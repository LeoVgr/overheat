using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour
{
    public GameObject Target;

    private Vector3 _offset = Vector3.zero;

    private void Start()
    {
        _offset = this.transform.position - Target.transform.position;
    }

    private void FixedUpdate()
    {
        this.transform.position = Target.transform.position + _offset;
    }
}
