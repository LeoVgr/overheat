using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeActivator : MonoBehaviour
{
    public GameObject StopRobot;
    public GameObject StopOtherRobot;
    public GameObject Generator;
    public GameObject Collider;
    public GameObject ColliderOnRoad;
    public GameObject RobotSpawner;
    public GameObject DoorChecker;

    // Start is called before the first frame update
    void Start()
    {
        ColliderOnRoad.SetActive(false);
        RobotSpawner.SetActive(true);
        StopRobot.SetActive(false);
        StopOtherRobot.SetActive(false);
        Collider.SetActive(true);
    }

    private void Update()
    {
        if (!Generator.activeInHierarchy && !DoorChecker.GetComponent<DoorChecker>().IsBlocked())
        {
            StopRobot.SetActive(true);
            RobotSpawner.SetActive(false);
        }

        if (StopRobot.GetComponent<StopRobot>().isThereARobot)
        {
            Collider.SetActive(false);
            StopOtherRobot.SetActive(true);
            ColliderOnRoad.SetActive(true);
        }
    }


}
