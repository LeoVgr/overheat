using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitSound : MonoBehaviour
{
    public AK.Wwise.Event FruitFallSound;
    public AK.Wwise.Event GrowSound;

    private bool _hasPlayed = false;

    public void PlayFruitSound()
    {
        if (!_hasPlayed)
        {
            _hasPlayed = true;
            FruitFallSound.Post(this.gameObject);
        }
        
    }
    public void PlayGrowSound()
    {
        GrowSound.Post(this.gameObject);

    }
    public void StopGrowSound()
    {
        GrowSound.Stop(this.gameObject);
    }

}
