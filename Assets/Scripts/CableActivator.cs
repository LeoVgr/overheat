using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CableActivator : MonoBehaviour
{
    public GameObject Cable;
    public GameObject Generator;

    private void Update()
    {
        if (!Generator.activeInHierarchy)
        {
            Cable.GetComponent<Renderer>().material.SetFloat("Vector1_ElectricPower", 0);
            this.gameObject.SetActive(false);
        }
    }
}
