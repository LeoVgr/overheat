using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(!other.GetComponent<Laser>() && (other.GetComponent<Rigidbody>() || other.GetComponent<BasicIA>()))
        {
            if (other.GetComponent<BasicIA>())
            {
                other.GetComponent<BasicIA>().StopRobot();
            }

            if (other.GetComponent<Growable>())
            {
                other.GetComponent<Growable>().growSound.Stop(other.gameObject);
            }
            Destroy(other.gameObject);
        }
            
    }
}
