using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorChecker : MonoBehaviour
{

    private List<GameObject> _colliders = new List<GameObject>();
    private bool _isBlocked = false;

    private void OnTriggerEnter(Collider other)
    {
        _colliders.Add(other.gameObject);
    }

    private void OnTriggerExit(Collider other)
    {
        _colliders.Remove(other.gameObject);
    }

    public bool IsBlocked()
    {
        return _colliders.Count != 0;
    }
}
