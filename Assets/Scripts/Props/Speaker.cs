using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class Speaker : InteractableObject
{
    public AK.Wwise.Event SpeakerMusic;
    public Mesh DestroyedSpeaker;
    public VisualEffect VfxSmoke;

    private void Awake()
    {
        VfxSmoke.enabled = false;
        
    }
    public override void Start()
    {
        base.Start();
        SpeakerMusic.Post(this.gameObject);
    }
    public override void Destroy()
    {
        base.Destroy();
        this.GetComponent<MeshFilter>().sharedMesh = DestroyedSpeaker;
        VfxSmoke.enabled = true;
    }

    public override void EndOfDurability()
    {
        DestructionSound.Post(this.gameObject);
        SpeakerMusic.Stop(this.gameObject);
        Destroy();
    }
}
