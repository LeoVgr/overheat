using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructibleObject : InteractableObject
{
    public bool isDestructibleWithLaser = true;
    public float MassChildren;

    public override void Update()
    {
        base.Update();
    }
    public override void GetHurt()
    {
        //Damage the object only if it can be damaged by laser
        if (isDestructibleWithLaser)
        {
            base.GetHurt();
        }
            
    }
    public override void Destroy()
    {
        base.Destroy();
        DestructionSound.Post(this.gameObject);
    }
    public override void EndOfDurability()
    {
        if(this.gameObject.GetComponent<MeshCollider>())
            this.gameObject.GetComponent<MeshCollider>().enabled = false;

        if (this.gameObject.GetComponent<BoxCollider>())
            this.gameObject.GetComponent<BoxCollider>().enabled = false;

        for (int i = 0; i < this.transform.childCount; i++)
        {
            if(!this.transform.GetChild(i).gameObject.GetComponent<MeshCollider>())
                this.transform.GetChild(i).gameObject.AddComponent<MeshCollider>();

            this.transform.GetChild(i).gameObject.GetComponent<MeshCollider>().convex = true;

            if(!this.transform.GetChild(i).gameObject.GetComponent<Rigidbody>())
                this.transform.GetChild(i).gameObject.AddComponent<Rigidbody>();
            this.transform.GetChild(i).gameObject.GetComponent<Rigidbody>().mass = MassChildren;
        }

        Destroy();
    }
    
}
