using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Activator : MonoBehaviour
{
    public GameObject Interuptor;
    public AK.Wwise.Event InteruptorSound;
    public AK.Wwise.Event DoorSound;

    private bool _isActivated = false;

    private void Update()
    {
        if (!Interuptor.activeInHierarchy && !_isActivated)
        {
            this.GetComponent<Animator>().SetTrigger("Open");

            //Sounds 
            InteruptorSound.Post(Interuptor);
            DoorSound.Post(this.gameObject);

            _isActivated = true;
        }
    }
}
