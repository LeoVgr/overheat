using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alarm : MonoBehaviour
{
    #region "Attributs"
    public float ThresholdPercentDestruction = 50f;
    public float RangeDetection = 5f;
    public AK.Wwise.Event AlarmSound;
    public  Animation _animationClip;
    public GameObject Lights;
    public Material OffMaterial;
    public Material OnMaterial;

    private List<InteractableObject> _detectedObjects = new List<InteractableObject>();
    private bool _isOn = false;
    #endregion

    #region "Events"
    // Start is called before the first frame update
    void Start()
    {
        Lights.SetActive(false);

        //Change material
        Material[] intMaterials = new Material[this.GetComponentInChildren<MeshRenderer>().materials.Length];
        for (int i = 0; i < intMaterials.Length; i++)
        {
            intMaterials[i] = this.GetComponentInChildren<MeshRenderer>().materials[i];
        }
        intMaterials[0] = OffMaterial;

        this.GetComponentInChildren<MeshRenderer>().materials = intMaterials;

        //Looking for nearby props
        Collider[] colliders = Physics.OverlapSphere(this.transform.position, RangeDetection);

        foreach (Collider hit in colliders)
        {
            InteractableObject obj = hit.GetComponent<InteractableObject>();

            if (obj != null && obj!= this.gameObject)
            {
                _detectedObjects.Add(obj);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!_isOn)
        {
            float totalProps = _detectedObjects.Count;
            float destroyedProps = 0;
            foreach (InteractableObject props in _detectedObjects)
            {

                if (props.GetIsDestroyed())
                {
                    ++destroyedProps;
                }
            }

            //If the destruction thereshold is lower than the actuel destruction percent
            if ((destroyedProps * 100f) / totalProps >= ThresholdPercentDestruction)
                SetOnAlarm();
        }
        
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(this.transform.position, RangeDetection);
    }
    #endregion

    #region "Methods"
    private void SetOnAlarm()
    {
        Lights.SetActive(true);

        //Change material
        Material[] intMaterials = new Material[this.GetComponentInChildren<MeshRenderer>().materials.Length];
        for (int i = 0; i < intMaterials.Length; i++)
        {
            intMaterials[i] = this.GetComponentInChildren<MeshRenderer>().materials[i];
        }
        intMaterials[0] = OnMaterial;

        this.GetComponentInChildren<MeshRenderer>().materials = intMaterials;

        _animationClip.Play();

        AlarmSound.Post(this.gameObject);

        _isOn = true;
    }
    #endregion

}
