using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConveyorBelt : MonoBehaviour
{
    #region "Attributs"
    public GameObject PrefabConveyorBelt;
    public float Speed;
    public float Spacing;
    public GameObject StartingPoint;
    public GameObject EndingPoint;
    public float TimeObjectSpawn;
    public List<GameObject> PullObject;

    private Vector3 _direction;
    private List<GameObject> _pull = new List<GameObject>();
    private Rigidbody _rigidbody;
    private float _timer;
    #endregion

    #region "Events"
    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();

        //Initialize the conveyor by creating all the parts on the track
        float size = (EndingPoint.transform.position - StartingPoint.transform.position).magnitude;
        int numberOfPart = (int)(size / (PrefabConveyorBelt.GetComponent<Renderer>().bounds.size.z + Spacing));
        _direction = (EndingPoint.transform.position - StartingPoint.transform.position).normalized;

        for (int i = 0; i <= numberOfPart; i++)
        {
            Vector3 offset = _direction * (PrefabConveyorBelt.GetComponent<Renderer>().bounds.size.z + Spacing) * i;
            GameObject part = Instantiate(PrefabConveyorBelt, StartingPoint.transform.position + offset, this.transform.rotation * Quaternion.AngleAxis(90, Vector3.up));
            part.transform.SetParent(this.transform);
            part.name = part.name + i;
            _pull.Add(part);
        }      
    }
    private void FixedUpdate()
    {
        Vector3 position = _rigidbody.position;
        _rigidbody.position += -_direction * Speed * Time.fixedDeltaTime;
        _rigidbody.MovePosition(position);
    }
    private void Update()
    {
        //Move the parts
        Vector3 movement = _direction * Speed;
        foreach (GameObject part in _pull)
        {
            part.transform.Translate(movement * Time.deltaTime, Space.World);

            //Despawn and respawn part
            if ((EndingPoint.transform.position - StartingPoint.transform.position).magnitude < (part.transform.position - StartingPoint.transform.position).magnitude &&
                (NextPart(part).transform.position - StartingPoint.transform.position).magnitude >= (PrefabConveyorBelt.GetComponent<Renderer>().bounds.size.z + Spacing))
            {
                part.transform.position = StartingPoint.transform.position;
            }
        }

        //Spawn items on belt
        _timer += Time.deltaTime;
        if(_timer >= TimeObjectSpawn)
        {
            SpawnObjectOnBelt();
            _timer = 0;
        }

    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawCube(StartingPoint.transform.position, new Vector3(0.5f,0.5f,0.5f));
        Gizmos.DrawCube(EndingPoint.transform.position, new Vector3(0.5f, 0.5f, 0.5f));
    }
    #endregion

    #region "Methods"
    private GameObject NextPart(GameObject part)
    {
        for (int i = 0; i < _pull.Count; i++)
        {
            if(part == _pull[i])
            {
                if (i != _pull.Count-1)
                {
                    return _pull[i + 1];
                }
                else
                {
                    return _pull[0];
                }
            }
        }
        return null;
    }
    private void SpawnObjectOnBelt()
    {
        //Pick a random object and spawn it on the belt
        if (PullObject.Count > 0)
        {
            int randomNumber = Random.Range(0, PullObject.Count);
            Vector3 SpawnPoint = StartingPoint.transform.position + _direction + new Vector3(0, PullObject[randomNumber].GetComponent<Renderer>().bounds.size.y / 2f + PrefabConveyorBelt.GetComponent<Renderer>().bounds.size.y / 2f + 0.1f, 0);

            Instantiate(PullObject[randomNumber], SpawnPoint, this.transform.rotation);
        }
            
    }
    #endregion

}
