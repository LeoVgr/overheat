using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public abstract class InteractableObject : MonoBehaviour
{

    #region "Attributs"
    //static attributs to evaluate damage
    public static int TotalProps = 0;
    public static int DestroyedProps = 0;

    public float MaxDurability;
    protected float CurrentDurability;
    protected bool IsDestroyed = false;
    protected bool IsHurted = false;
    protected bool IsHurtedByExplosion = true;

    [Space]
    [Header("Sounds")]
    public AK.Wwise.Event ImpactSound;
    public AK.Wwise.Event CollisionSound;
    public AK.Wwise.Event DestructionSound;
    protected bool _isSoundPlaying = false;
    #endregion

    #region "Events"
    public virtual void Start()
    {
        CurrentDurability = MaxDurability;

        if (SceneManager.GetActiveScene().buildIndex == 4)//Factory build index
        {
            ++TotalProps;
        }
        else
        {
            TotalProps = 0;
            DestroyedProps = 0;
        }
        
    }
    public void LateUpdate()
    {
        UpdateSound();
    }
    public virtual void Update()
    {
        IsHurted = false;
    }
    #endregion

    #region "Methods"
    public bool GetIsDestroyed()
    {
        return IsDestroyed;
    }
    public virtual void GetHurt()
    {
        CurrentDurability -= Time.deltaTime;
        IsHurted = true;

        if (CurrentDurability <= 0 && !IsDestroyed)
        {
           IsDestroyed = true;
           EndOfDurability();
        }
    }
    public virtual void ApplyDamage(float _damages)
    {
        if (IsHurtedByExplosion)
        {
            CurrentDurability = Mathf.Max(0, CurrentDurability - _damages);

            if (CurrentDurability <= 0 && !IsDestroyed)
            {
                IsDestroyed = true;
                EndOfDurability();
            }
        }
        
    }
    public abstract void EndOfDurability();
    public virtual void Destroy()
    {
        if (SceneManager.GetActiveScene().buildIndex == 4)//Factory build index
        {
            ++DestroyedProps;
        }
    }
    private void UpdateSound()
    {
        
        if (IsHurted)
        {
            if (!_isSoundPlaying)
            {
                CollisionSound.Post(this.gameObject);
                _isSoundPlaying = true;
            }
        }
        else
        {
            if (_isSoundPlaying)
            {
                CollisionSound.Stop(this.gameObject);
                _isSoundPlaying = false;
            }
        }

    }
    private void OnCollisionEnter(Collision collision)
    {
        ImpactSound.Post(this.gameObject);
    }

    #endregion

}
