using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class MeltableObject : InteractableObject
{
    #region "Attributs"
    public VisualEffect SmokeVfx;
    public float Melting;
    public Transform DestructionPoint;

    private float _meltingFactor;
    private bool _isSmoking;
    private SkinnedMeshRenderer _skinnedMeshRenderer;
    private Mesh _tempMesh1;
    private Mesh _tempMesh2;
    private bool _isTempMesh1 = true;
    #endregion

    #region "Events"
    public override void Start()
    {
        IsHurtedByExplosion = false;
        base.Start();
        _meltingFactor = Melting;
        _skinnedMeshRenderer = this.GetComponent<SkinnedMeshRenderer>();
        _isSmoking = false;
        SmokeVfx.Stop();
    }
    public override void Update()
    {
        base.Update();
        UpdateMeltingState();
    }
    #endregion

    #region "Methods"
    public override void EndOfDurability()
    {
        Destroy();
    }
    private void UpdateMeltingState()
    {
        if(!_isSmoking && CurrentDurability < MaxDurability)
        {
            SmokeVfx.Play();
            _isSmoking = true;
        }            

        //If eyes of player are higher than the max height of the props, make it melt 
        if (this.gameObject.GetComponent<MeshCollider>().bounds.max.y <= DestructionPoint.position.y + 0.10f && CurrentDurability != MaxDurability)
        {         
           GetHurt();
           IsHurted = false;
        }

        _meltingFactor = Melting - ((CurrentDurability * Melting) / MaxDurability);

        ////Update animation
        _skinnedMeshRenderer.SetBlendShapeWeight(0, _meltingFactor);
        this.GetComponent<Renderer>().material.SetFloat("Vector1_Heat", _meltingFactor / Melting);

        //Update mesh collider
        var collider = GetComponent<MeshCollider>();
        if (_isTempMesh1)
        {
            _isTempMesh1 = false;
            _tempMesh1 = new Mesh();
            _skinnedMeshRenderer.BakeMesh(_tempMesh1);
            collider.sharedMesh = _tempMesh1;
            Destroy(_tempMesh2);
        }
        else
        {
            _isTempMesh1 = true;
            _tempMesh2 = new Mesh();
            _skinnedMeshRenderer.BakeMesh(_tempMesh2);
            collider.sharedMesh = _tempMesh2;
            Destroy(_tempMesh1);
        }
    
        //_skinnedMeshRenderer.BakeMesh(_mesh);
       // var collider = GetComponent<MeshCollider>();
        //collider.sharedMesh = bakeMesh;
        //collider.sharedMesh = _mesh;
        //Destroy(_mesh);


    }
    public override void Destroy()
    {
        base.Destroy();
        DestructionSound.Post(this.gameObject);
        this.gameObject.SetActive(false);
    }
   
    #endregion
}
