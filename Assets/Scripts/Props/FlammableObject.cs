using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class FlammableObject : InteractableObject
{
    #region "Attributs"
    public VisualEffect FireVfx;
    public VisualEffect DestructionVfx;
    public float ExposureMaxDistance; //Max distance to affect other flammable object
    public float FlammableTime; //Combustion time
    public float ExposureAmmount; //Ammount needed to this object to be in fire when exposed with another flammable object
    public float BurnFactorWhenHurt = 3f;
    public bool IsOnFire;
    public bool AddRBToChildren = false;
    public bool enableChildrenCollider = false;
    public bool disableChildrenAfterBurn = false;
    public float MassChildren = 1;

    private float _maxExposureAmount;
    private float _maxLife = 0;
    private float _currentLife;

    [Space]
    [Header("Sounds")]
    public AK.Wwise.Event FireSound;
    #endregion

    #region "Events"
    public override void Start()
    {
        base.Start();

        _maxExposureAmount = ExposureAmmount;
        _maxLife = FlammableTime + MaxDurability;
        _currentLife = _maxLife;
        IsOnFire = false;
        FireVfx.Stop();
        FireVfx.SetFloat("Lifetime",0);       
    }
    public override void Update()
    {
        if (IsOnFire)
        {
            SpreadFire();
        }

        if (IsOnFire && FlammableTime > 0)
        {
            if (IsHurted)
            {
                FlammableTime -= Time.deltaTime * BurnFactorWhenHurt;

                if (FlammableTime <= 0)
                    Destroy();
            }
            else
            {
                FlammableTime -= Time.deltaTime;

                if (FlammableTime <= 0)
                    Destroy();
            }

        }

        AkSoundEngine.SetRTPCValue("GP_Woodbox_Health", (CurrentDurability * 100) / MaxDurability, this.gameObject);

        //Update ashes shader
        _currentLife = FlammableTime + CurrentDurability;
        this.GetComponent<Renderer>().material.SetFloat("Vector1_AshesFactor", (_currentLife / _maxLife));

        base.Update();

    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(this.transform.position, ExposureMaxDistance);
    }
    #endregion

    #region "Methods"
    private void SpreadFire()
    {
        //Looking for nearby props
        Vector3 fireSource = this.transform.position;
        Collider[] colliders = Physics.OverlapSphere(fireSource, ExposureMaxDistance);

        foreach (Collider hit in colliders)
        {
            if(hit.gameObject.GetComponent<FlammableObject>())
                hit.gameObject.GetComponent<FlammableObject>().ExposedToFire();
        }
    }
    public override void EndOfDurability()
    {
        SetOnFire();
    }
    public void ExposedToFire()
    {
        ExposureAmmount -= Time.deltaTime;
        if (ExposureAmmount < 0)
            SetOnFire();
    }    
    private void SetOnFire()
    {
        
        if (!IsOnFire)
        {
            FireVfx.Play();
            FireVfx.SetFloat("Lifetime", 20);
            IsOnFire = true;

            FireSound.Post(this.gameObject);
        }
        
    }
    public override void Destroy()
    {
        base.Destroy();
        DestructionVfx.Play();
        DestructionSound.Post(this.gameObject);

        //Disable renderer, collider and fire
        FireVfx.enabled = false;
        this.GetComponent<Renderer>().enabled = false;
        this.GetComponent<Collider>().enabled = false;
        if(AddRBToChildren)
        {
            for (int i = 0; i < this.transform.childCount; i++)
            {
                Transform childTransform = transform.GetChild(i);
                if (childTransform.GetComponent<Collider>() != null)
                {
                    childTransform.gameObject.AddComponent<Rigidbody>();
                    childTransform.gameObject.GetComponent<Rigidbody>().mass = MassChildren;
                    childTransform.gameObject.GetComponent<Collider>().enabled = true;
                    childTransform.gameObject.GetComponent<OnDestroyAfterTime>()?.DestroyChildrenAfterTime();
                }
            }
        }
        if (disableChildrenAfterBurn)
        {
            for (int i = 0; i < this.transform.childCount; i++)
            {
                Transform childTransform = transform.GetChild(i);
                if ((childTransform.gameObject != FireVfx.gameObject) && (childTransform.gameObject != DestructionVfx.gameObject))
                {
                    childTransform.gameObject.SetActive(false);
                    Debug.LogError("LULZ");
                }
            }
        }
    }
    public void SwitchOffFire()
    {
        IsOnFire = false;
        FireVfx.Stop();
        FireVfx.SetFloat("Lifetime", 0);
        this.CurrentDurability = MaxDurability;
        this.ExposureAmmount = _maxExposureAmount; 
        this.IsDestroyed = false;

    }   
    #endregion

}
