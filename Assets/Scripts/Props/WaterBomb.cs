using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class WaterBomb : InteractableObject
{
    #region "Attributs"
    public VisualEffect WaterBombVfx;
    public float RadiusExplosion;

    public AK.Wwise.Event ExplosiveSound;
    #endregion

    #region "Events"
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(this.transform.position, RadiusExplosion);
    }
    public override void Update()
    {
        base.Update();
    }
    #endregion

    #region "Methods"
    public override void Destroy()
    {
        base.Destroy();
        WaterBombVfx.Play();
        ExplosiveSound.Post(this.gameObject);

        //Disable renderer, collider and fire
        this.GetComponent<Renderer>().enabled = false;
        this.GetComponent<Collider>().enabled = false;
    }
    public override void EndOfDurability()
    {

        //Looking for nearby props
        Vector3 _explosionSource = this.transform.position;
        Collider[] _colliders = Physics.OverlapSphere(_explosionSource, RadiusExplosion);

        foreach (Collider hit in _colliders)
        {
            if (hit.GetComponent<FlammableObject>())
            {
                hit.GetComponent<FlammableObject>().SwitchOffFire();
            }
        }

        Destroy();
    }
    #endregion

    

}
