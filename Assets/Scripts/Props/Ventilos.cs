using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class Ventilos : MonoBehaviour
{
    #region "Attributs"
    public float WindPower;

    private BoxCollider _trigger;
    #endregion

    #region "Events"
    private void Start()
    {
        _trigger = GetComponent<BoxCollider>();
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.GetComponent<Rigidbody>())
        {
            other.GetComponent<Rigidbody>().AddForce( this.transform.right.normalized * WindPower);
        }
    }
    #endregion

}
