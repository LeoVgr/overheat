using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class Growable : MonoBehaviour
{
    #region "Attributs"
    public float MaxGrowAmmount;
    public List<float> MaxGrowFactorShader;
    public bool StartAlreadyGrown;
    public VisualEffect VfxGrow;

    private bool _isGrowingVfxPlaying = false;
    private bool _isGrown = false;
    private float _currentGrow;

    [Space]
    [Header("Sounds")]
    public AK.Wwise.Event growSound;
    public AK.Wwise.Event endGrowSound;
    private bool _isGrowSoundPlaying = false;
    #endregion

    #region "Events"
    private void Start()
    {
        //Start at his max size
        if (StartAlreadyGrown)
        {
            _currentGrow = MaxGrowAmmount;
            _isGrown = true;
        }
        else
        {
            _currentGrow = 0;          
        }

        //Update Shaders
        UpdateGrowShader();

        VfxGrow.Stop();
    }
    #endregion

    #region "Methods"
    public void Grow(float p_value)
    {
        if (!_isGrowingVfxPlaying)
        {
            _isGrowingVfxPlaying = true;
            VfxGrow.Play();
        }

        if (!_isGrown)
        {
            _currentGrow = Mathf.Min(MaxGrowAmmount, _currentGrow + p_value);
            UpdateGrowShader();

            //Play grow sound
            if (!_isGrowSoundPlaying)
            {
                _isGrowSoundPlaying = true;
                growSound.Post(this.gameObject);
            }

            //Play End sound
            if (_currentGrow == MaxGrowAmmount)
            {
                if (!_isGrown)
                {
                    endGrowSound.Post(this.gameObject);
                    _isGrown = true;
                    VfxGrow.Stop();
                }

            }
        }             
    }
    public void StopGrow()
    {
        if (_isGrowSoundPlaying)
        {
            _isGrowSoundPlaying = false;
            growSound.Stop(this.gameObject);
        }

        if (_isGrowingVfxPlaying)
        {
            _isGrowingVfxPlaying = false;
            VfxGrow.Stop();
        }
        
    }
    private void UpdateGrowShader()
    {
        int i = 0;
        foreach (Material material in this.GetComponent<Renderer>().materials)
        {
            if (i < MaxGrowFactorShader.Count)
            {
                material.SetFloat("Vector1_GrowingFactor", MaxGrowFactorShader[i] - ((_currentGrow * MaxGrowFactorShader[i]) / MaxGrowAmmount));
                ++i;
            }
        }
    }
    #endregion

}
