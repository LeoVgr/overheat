using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.UI;

public class CameraSpot : MonoBehaviour
{
    #region "Attributs"
    public Animator UIIndications;
    public GameObject UIIndicationGamepad;
    public GameObject UIIndicationKeyboard;
    public GameObject Ui;
    public GameObject Player;
    public CinemachineVirtualCamera VirtualCameraPlayer;
    public CinemachineVirtualCamera VirtualCameraLevel;
    public float CameraSpeed;
    public AK.Wwise.Event StartSoundCameraSpot;
    public AK.Wwise.Event StopSoundCameraSpot;

    private float _coolDown = 0.8f;
    private float _timer = 0f;
    private bool _isOnCd = false;
    private Vector3 _forward = Vector3.zero;
    private Vector3 _right = Vector3.zero;
    private bool _onCameraSpot = false;
    private bool _isInPlayerCam = true;
    #endregion

    #region "Events"
    private void Start()
    {
        _forward = Camera.main.transform.forward;
        _forward.y = 0;
        _forward = Vector3.Normalize(_forward);
        _right = Vector3.Normalize(Quaternion.Euler(new Vector3(0, 90, 0)) * _forward);

        Ui.SetActive(false);
        UIIndications.gameObject.SetActive(false);

        UIIndicationGamepad.SetActive(InputManager.instance.IsUsingGamepad());
        UIIndicationKeyboard.SetActive(!InputManager.instance.IsUsingGamepad());


    }
    private void Update()
    {
        if (_isOnCd)
        {
            _timer += Time.deltaTime;

            if(_timer >= _coolDown)
            {
                _timer = 0f;
                _isOnCd = false;
            }
        }

        if (_onCameraSpot && InputManager.instance.IsInteractAction())
        {
            SwitchPriority();         
        }

        if (InputManager.instance.IsInteractAction() && !_isInPlayerCam)
        {
            SwitchPriority();
            Player.GetComponent<Laser>().SwitchLaserOn();
            InputManager.instance.EnableInputs();
        }
            

        if (_onCameraSpot)
        {
            //Disable laser and player movement
            if (!_isInPlayerCam)
            {
                Player.GetComponent<Laser>().SwitchLaserOff();
                InputManager.instance.DisableInputs();
            }
            else
            {
                Player.GetComponent<Laser>().SwitchLaserOn();
                InputManager.instance.EnableInputs();
            }
        }

        if (!_isInPlayerCam)
        {
            MoveLevelCamera();
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == Player)
        {
            UIIndications.gameObject.SetActive(true);
            UIIndications.SetBool("Open",true);
            _onCameraSpot = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == Player)
        {
            _onCameraSpot = false;
            UIIndications.SetBool("Open", false);
        }
    }
    #endregion

    #region "Methods"
    private void SwitchPriority()
    {
        if (_isInPlayerCam)
        {
            if (!_isOnCd)
            {
                _isOnCd = true;
                VirtualCameraLevel.Priority = 15;
                _isInPlayerCam = false;
                Ui.SetActive(true);
                StartSoundCameraSpot.Post(this.gameObject);


            }
            
        }
        else
        {
            if (!_isOnCd)
            {
                _isOnCd = true;
                VirtualCameraLevel.Priority = 9;
                _isInPlayerCam = true;
                Ui.SetActive(false);
                StopSoundCameraSpot.Post(this.gameObject);
            }          
        }
        
    }
    private void MoveLevelCamera()
    {
        Vector3 rightMovement = _right * InputManager.instance.GetInputMovementVector().x;
        Vector3 upMovement = _forward * InputManager.instance.GetInputMovementVector().y;

        VirtualCameraLevel.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset += (rightMovement + upMovement).normalized * CameraSpeed * Time.deltaTime;
    }
    #endregion
    
}
