using MoreMountains.Feedbacks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class ExplosiveObject : InteractableObject
{
    #region "Attributs"
    public float MassChildren;
    public Vector3 CenterExplosion;
    public float DamageExplosion = 5f;
    public float RadiusExplosion = 5.0F;
    public float PowerExplosion = 10.0F;
    public float UpwardExplosionModifier = 3.0f;

    [Space]
    [Header("FeedBacks")]
    public bool DisactivateParentMesh = false;
    public VisualEffect ExplosionVfx;
    public MMFeedbacks ExplosionFeedback;
    public float TimeIntervalFeedbacks;
    public float IntensityControllerVibration;
    public float DurationControllerVibration;

    [Space]
    [Header("Sounds")]
    public AK.Wwise.Event ExplosiveSound;
    #endregion

    #region "Events"
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(this.transform.position + CenterExplosion, RadiusExplosion);
        Gizmos.DrawSphere(this.transform.position + CenterExplosion, 0.1f);
    }
    public override void Update()
    {
        base.Update();
    }
    #endregion

    #region "Methods"
    public override void EndOfDurability()
    {
        PlayFeedbacks();
        Invoke("Explosion", TimeIntervalFeedbacks);         
    }
    private void Explosion()
    {
        //Feedbacks
        ExplosionVfx?.Play();
        ExplosiveSound.Post(this.gameObject);

        //Destroy the object
        if (this.gameObject.GetComponent<MeshCollider>())
            Destroy(this.gameObject.GetComponent<MeshCollider>());

        if(DisactivateParentMesh)
        {
            this.gameObject.GetComponent<MeshRenderer>().enabled = false;
		}

        if (this.gameObject.GetComponent<BoxCollider>())
            Destroy(this.gameObject.GetComponent<BoxCollider>());

        for (int i = 0; i < this.transform.childCount; i++)
        {
            this.transform.GetChild(i).gameObject.AddComponent<Rigidbody>();
            this.transform.GetChild(i).gameObject.GetComponent<Rigidbody>().mass = MassChildren;
            this.transform.GetChild(i).gameObject.AddComponent<MeshCollider>();
            this.transform.GetChild(i).gameObject.GetComponent<MeshCollider>().convex = true;
        }

        //Looking for nearby props
        Vector3 explosionSource = this.transform.position + CenterExplosion;
        Collider[] colliders = Physics.OverlapSphere(explosionSource, RadiusExplosion);

        //Used to simulate explosion on children of destroyable object
        List<GameObject> destroyableObjects = new List<GameObject>();

        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();

            //Add do the list destroyable object to apply after an explosion force on their children
            if (hit.GetComponent<DestructibleObject>())
            {
                destroyableObjects.Add(hit.gameObject);
            }

            if (rb != null)
            {
                rb.AddExplosionForce(PowerExplosion, explosionSource, RadiusExplosion, UpwardExplosionModifier);
            }

            //Apply damage
            InteractableObject interactableObject = hit.transform.GetComponent<InteractableObject>();
            if (interactableObject && !hit.transform.GetComponent<MeltableObject>()) interactableObject.ApplyDamage(DamageExplosion);
        }

        //Apply a second simulated explosion on children of destructible object
        foreach (GameObject go in destroyableObjects)
        {
            for (int i = 0; i < go.transform.childCount; i++)
            {
                if (go.transform.GetChild(i).GetComponent<Rigidbody>())
                {
                    go.transform.GetChild(i).GetComponent<Rigidbody>().AddExplosionForce(PowerExplosion, explosionSource, RadiusExplosion, UpwardExplosionModifier);
                }
            }
        }

        Destroy();
    }
    private void PlayFeedbacks()
    {
        //Play feedbacks   
        ExplosionFeedback?.PlayFeedbacks(this.transform.position, 1);
    }

    //public override void Destroy()
    //{
    //    base.Destroy();
    //    //this.gameObject.SetActive(false);
    //}
    #endregion


}
