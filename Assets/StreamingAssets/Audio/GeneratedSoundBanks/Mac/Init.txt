Game Parameter	ID	Name			Wwise Object Path	Notes
	13924921	GP_Volume_Master_Sliders			\Default Work Unit\GP_Volume_Master_Sliders	
	504532776	Brick			\Factory Acoustic Textures\Textures\Brick	
	513139656	Mountain			\Factory Acoustic Textures\Textures\Mountain	
	841620460	Concrete			\Factory Acoustic Textures\Textures\Concrete	
	1755085759	Wood_Deep			\Factory Acoustic Textures\Textures\Wood_Deep	
	1873957695	Anechoic			\Factory Acoustic Textures\Textures\Anechoic	
	1970351858	Fabric			\Factory Acoustic Textures\Textures\Fabric	
	2058049674	Wood			\Factory Acoustic Textures\Textures\Wood	
	2412606308	Carpet			\Factory Acoustic Textures\Textures\Carpet	
	2637588553	Tile			\Factory Acoustic Textures\Textures\Tile	
	2928161104	Curtains			\Factory Acoustic Textures\Textures\Curtains	
	3195498748	Cork_Tiles			\Factory Acoustic Textures\Textures\Cork_Tiles	
	3670307564	Drywall			\Factory Acoustic Textures\Textures\Drywall	
	4168643977	Acoustic_Banner			\Factory Acoustic Textures\Textures\Acoustic_Banner	
	4262522749	Wood_Bright			\Factory Acoustic Textures\Textures\Wood_Bright	

Audio Bus	ID	Name			Wwise Object Path	Notes
	3803692087	Master Audio Bus			\Default Work Unit\Master Audio Bus	

Auxiliary Bus	ID	Name			Wwise Object Path	Notes
	282149398	Delay_Room2			\Default Work Unit\Master Audio Bus\Delay_Room2	
	508475202	Reverb_Props			\Default Work Unit\Master Audio Bus\Reverb_Props	
	1076479569	Reverb_Laser			\Default Work Unit\Master Audio Bus\Reverb_Laser	
	2729821376	Reverb_PNJ			\Default Work Unit\Master Audio Bus\Reverb_PNJ	
	3676793708	Reverb_HP_Music			\Default Work Unit\Master Audio Bus\Reverb_HP_Music	

Effect plug-ins	ID	Name	Type				Notes
	373991612	Wwise Recorder (Custom)	Wwise Recorder			
	506891147	Plate_Small	Wwise RoomVerb			
	1588715066	Echoes_Linear	Wwise Delay			
	1706684651	Plate_Medium	Wwise RoomVerb			
	2995775460	Room_Small	Wwise RoomVerb			

Audio Devices	ID	Name	Type				Notes
	2317455096	No_Output	No Output			
	3859886410	System	System			

