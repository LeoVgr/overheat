/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID AMB_PLAY_AMB_MARAIS = 3306821097U;
        static const AkUniqueID EV_CHARA_FOOTSTEPS = 791418700U;
        static const AkUniqueID EV_PLAY_AMB_BIGROOM_PROD = 2385245066U;
        static const AkUniqueID EV_PLAY_AMB_COULOIR = 2986699072U;
        static const AkUniqueID EV_PLAY_AMB_FACTORY_ROOM1_TUTO = 3180902349U;
        static const AkUniqueID EV_PLAY_AMB_FACTORY_ROOM2_SERRE = 1806381947U;
        static const AkUniqueID EV_PLAY_AMB_FACTORY_ROOM3_PROD = 2998331134U;
        static const AkUniqueID EV_PLAY_AMB_MECANO_ROOM = 1391182742U;
        static const AkUniqueID EV_PLAY_AMB_WATER_TURBINE = 537571768U;
        static const AkUniqueID EV_PLAY_ARM_BACKWARD = 3116821426U;
        static const AkUniqueID EV_PLAY_ARM_METAL_IMPACT = 1563265563U;
        static const AkUniqueID EV_PLAY_ARM_MOVEMENT2 = 2040650376U;
        static const AkUniqueID EV_PLAY_ARM_UP = 4016946450U;
        static const AkUniqueID EV_PLAY_BARREL_EXPLOSION = 1062425696U;
        static const AkUniqueID EV_PLAY_BRIDGY_ROBOT_START = 3888482671U;
        static const AkUniqueID EV_PLAY_BRIDGY_ROBOT_STOP = 4000918645U;
        static const AkUniqueID EV_PLAY_CHARA_UV_LIGHT_ROBOT = 621133417U;
        static const AkUniqueID EV_PLAY_CONVEYORBELT = 2043666644U;
        static const AkUniqueID EV_PLAY_CRANERS_ROBOT_DAMAGE = 2047286125U;
        static const AkUniqueID EV_PLAY_CRANERS_ROBOT_ENGINE = 3532990976U;
        static const AkUniqueID EV_PLAY_DOOR_OPEN = 905010263U;
        static const AkUniqueID EV_PLAY_FINGER_CLAP = 1223792072U;
        static const AkUniqueID EV_PLAY_FOL_BUMP_METAL_RND = 4224297771U;
        static const AkUniqueID EV_PLAY_FOL_BUMP_WOOD_RND = 3127478423U;
        static const AkUniqueID EV_PLAY_GENERATEUR_IMPACT = 2242647157U;
        static const AkUniqueID EV_PLAY_GENERATEUR_STATIC = 1448959985U;
        static const AkUniqueID EV_PLAY_GRASSY_ROBOT_CUT = 3366645743U;
        static const AkUniqueID EV_PLAY_GRASSY_ROBOT_ENGINE = 1085691331U;
        static const AkUniqueID EV_PLAY_HAND1 = 1940473172U;
        static const AkUniqueID EV_PLAY_HEAD_MOVEMENT2AND3 = 3309696698U;
        static const AkUniqueID EV_PLAY_HEAD_MOVEMENT_LITTLE = 3173633697U;
        static const AkUniqueID EV_PLAY_MECANIC_BRIDGE_DOWN = 3196129263U;
        static const AkUniqueID EV_PLAY_MECANIC_BRIDGE_UP = 2568496600U;
        static const AkUniqueID EV_PLAY_METAL_SMALL_IMPACT = 1367678358U;
        static const AkUniqueID EV_PLAY_MUSHROOMBOX_EXPLOSION = 958637417U;
        static const AkUniqueID EV_PLAY_MUSIC_PROD = 1556415823U;
        static const AkUniqueID EV_PLAY_MUSIC_TUTO_CREDIT = 1189294534U;
        static const AkUniqueID EV_PLAY_MUSIC_TUTO_MONO = 753014420U;
        static const AkUniqueID EV_PLAY_PROPS_LASER_MELT__COLLISION = 513175003U;
        static const AkUniqueID EV_PLAY_PROPS_LASER_MELT__COLLISION_DISPARITION = 3303178830U;
        static const AkUniqueID EV_PLAY_PROPS_LASER_PLANT_COLLISION = 1205594793U;
        static const AkUniqueID EV_PLAY_PROPS_PLANT_IMPACT_DISPARITION = 1454742776U;
        static const AkUniqueID EV_PLAY_ROBOT_DESTRUCTION = 4116539325U;
        static const AkUniqueID EV_PLAY_ROBOT_IS_REPAIR = 3392453045U;
        static const AkUniqueID EV_PLAY_ROBOT_MECANO_ARMBACK = 1884879934U;
        static const AkUniqueID EV_PLAY_ROBOT_MECANO_ARMBACK_PHASE2 = 902992040U;
        static const AkUniqueID EV_PLAY_ROBOT_MECANO_ARMDOWN = 3770457583U;
        static const AkUniqueID EV_PLAY_ROBOT_MECANO_HAND_PICK = 1101870248U;
        static const AkUniqueID EV_PLAY_ROBOT_MECANO_HAND_PICKALL = 429146105U;
        static const AkUniqueID EV_PLAY_ROBOT_MECANO_HAND_TURN = 2123700062U;
        static const AkUniqueID EV_PLAY_ROBOT_MECANO_HEAD_LITTLE_ZOOM = 1051591742U;
        static const AkUniqueID EV_PLAY_ROBOT_MECANO_HEAD_ZOOM = 3075418293U;
        static const AkUniqueID EV_PLAY_ROBOT_PNJ_BRIDGY_ENGINE = 1263190992U;
        static const AkUniqueID EV_PLAY_ROBOT_PNJ_FILTER_CLEANING = 3884579604U;
        static const AkUniqueID EV_PLAY_ROBOT_PNJ_FLY = 2838261587U;
        static const AkUniqueID EV_PLAY_ROBOT_PNJ_SPRINKLER = 2198339436U;
        static const AkUniqueID EV_PLAY_SFX_APPLE_FALL = 3822981844U;
        static const AkUniqueID EV_PLAY_SFX_FIRE_BURNS = 796352203U;
        static const AkUniqueID EV_PLAY_SFX_LASER_BASE = 1805259271U;
        static const AkUniqueID EV_PLAY_SFX_LASER_METAL_COLLISION = 3051037500U;
        static const AkUniqueID EV_PLAY_SFX_MUSHROOM_EXPLOSION = 182427318U;
        static const AkUniqueID EV_PLAY_SFX_PILLAR_DESTRUCTION = 3296094769U;
        static const AkUniqueID EV_PLAY_SFX_PLANT_IS_GROW = 1082826384U;
        static const AkUniqueID EV_PLAY_SFX_POUSSE_PLANT_RND = 2713881680U;
        static const AkUniqueID EV_PLAY_SFX_PROPS_ALARM = 736802268U;
        static const AkUniqueID EV_PLAY_SFX_PROPS_LASER_CONCRETE_COLLISION = 3776001625U;
        static const AkUniqueID EV_PLAY_SFX_PROPS_LASER_GLASS_COLLISION = 1550262306U;
        static const AkUniqueID EV_PLAY_SFX_PROPS_LASER_SPARKS_COLLISION = 4196954402U;
        static const AkUniqueID EV_PLAY_SFX_PROPS_LASER_WOODBOX_COLLISION = 1109113084U;
        static const AkUniqueID EV_PLAY_SFX_PROPS_METAL_FALL = 3470349354U;
        static const AkUniqueID EV_PLAY_SFX_PROPS_WOOD_FALL = 3013907614U;
        static const AkUniqueID EV_PLAY_SFX_PROPS_WOODBOX_DISPARITION = 2084541488U;
        static const AkUniqueID EV_PLAY_SFX_SPEAKER_EXPLOSION = 377526811U;
        static const AkUniqueID EV_PLAY_SFX_TREE_COLLISION = 64441545U;
        static const AkUniqueID EV_PLAY_SFX_VENTILLATION2 = 1877916701U;
        static const AkUniqueID EV_PLAY_SFX_WALL_DESTRUCTION = 410406921U;
        static const AkUniqueID EV_PLAY_SFX_WOODBOX_IMPACT = 2378706283U;
        static const AkUniqueID EV_PLAY_SFX_WOODBOX_SLIDE = 1539257684U;
        static const AkUniqueID EV_PLAY_UI_MENU_SELECTION = 1154401795U;
        static const AkUniqueID EV_PLAY_UI_MENU_VALIDATE = 3457163701U;
        static const AkUniqueID EV_PLAY_VOICE_ACCESS = 1178036723U;
        static const AkUniqueID EV_PLAY_VOICE_CAUTION = 2518253684U;
        static const AkUniqueID EV_PLAY_VOICE_DENIED = 3730906208U;
        static const AkUniqueID EV_PLAY_VOICE_OVERLOAD = 856557823U;
        static const AkUniqueID EV_PLAY_WATER_BARREL_EXPLOSION = 2622989984U;
        static const AkUniqueID EV_PLAY_WOODPIECE_IMPACT = 3446837632U;
        static const AkUniqueID EV_SROP_SFX_TREE_COLLISION = 4071193197U;
        static const AkUniqueID EV_STOP_ALL = 2751955999U;
        static const AkUniqueID EV_STOP_AMB_MECANO_ROOM = 224630252U;
        static const AkUniqueID EV_STOP_CRANERS_ROBOT_DAMAGE = 353964779U;
        static const AkUniqueID EV_STOP_CRANERS_ROBOT_ENGINE = 2872140806U;
        static const AkUniqueID EV_STOP_GRASSY_ROBOT_CUT = 2856371361U;
        static const AkUniqueID EV_STOP_GRASSY_ROBOT_ENGINE = 62147433U;
        static const AkUniqueID EV_STOP_MUSIC_PROD = 695303409U;
        static const AkUniqueID EV_STOP_MUSIC_TUTO_CREDIT = 4193905428U;
        static const AkUniqueID EV_STOP_MUSIC_TUTO_MONO = 1047511014U;
        static const AkUniqueID EV_STOP_PROPS_LASER_MELT__COLLISION = 506847509U;
        static const AkUniqueID EV_STOP_ROBOT_PNJ_FILTER_CLEANING = 199779106U;
        static const AkUniqueID EV_STOP_ROBOT_PNJ_FLY = 2196506129U;
        static const AkUniqueID EV_STOP_ROBOT_PNJ_SPRINKLER = 2061625162U;
        static const AkUniqueID EV_STOP_SFX_LASER_METAL_COLLISION = 1319146550U;
        static const AkUniqueID EV_STOP_SFX_PROPS_LASER_CONCRETE_COLLISION = 1992421907U;
        static const AkUniqueID EV_STOP_SFX_PROPS_LASER_GLASS_COLLISION = 1937390840U;
        static const AkUniqueID EV_STOP_SFX_WOODBOX_SLIDE = 3838302054U;
        static const AkUniqueID REC_CAMERA = 840391301U;
        static const AkUniqueID REC_CAMERA_STOP = 2818457608U;
        static const AkUniqueID STOP_AMB_BIGROOM_PROD = 469940718U;
    } // namespace EVENTS

    namespace SWITCHES
    {
        namespace ROBOT_CRANERS_CONDITIONS
        {
            static const AkUniqueID GROUP = 2293207693U;

            namespace SWITCH
            {
                static const AkUniqueID DAMAGE = 1786804762U;
                static const AkUniqueID NODAMAGE = 1078141303U;
            } // namespace SWITCH
        } // namespace ROBOT_CRANERS_CONDITIONS

        namespace SW_CHARA_BUMP_MATERIALS
        {
            static const AkUniqueID GROUP = 2456030937U;

            namespace SWITCH
            {
                static const AkUniqueID METAL = 2473969246U;
                static const AkUniqueID WOOD = 2058049674U;
            } // namespace SWITCH
        } // namespace SW_CHARA_BUMP_MATERIALS

        namespace SW_CHARA_FOOSTEPS_MATERIELS
        {
            static const AkUniqueID GROUP = 708098268U;

            namespace SWITCH
            {
                static const AkUniqueID DAMAGE_ROBOT = 101851955U;
                static const AkUniqueID NO_DAMAGE_ROBOT = 102818657U;
            } // namespace SWITCH
        } // namespace SW_CHARA_FOOSTEPS_MATERIELS

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID GP_DISTANCE_CRANERS_PNJ = 375479400U;
        static const AkUniqueID GP_DISTANCE_FILTER_ROBOT = 3356298762U;
        static const AkUniqueID GP_DISTANCE_GRASSY_PNJ = 1691368175U;
        static const AkUniqueID GP_DISTANCE_MUSIC_PROD = 2982796162U;
        static const AkUniqueID GP_DISTANCE_MUSIC_TUTO = 342824023U;
        static const AkUniqueID GP_DISTANCE_ROBOT_PNJ_FLY = 2695962940U;
        static const AkUniqueID GP_DISTANCE_SPRINKLER_PNJ = 1038674446U;
        static const AkUniqueID GP_LASER_DISTANCE_OBJECT = 1288130720U;
        static const AkUniqueID GP_LASER_MOVEMENT = 2252554852U;
        static const AkUniqueID GP_LASER_TIME_FOCUS = 4074375177U;
        static const AkUniqueID GP_MAINROBOT_MOVEMENT = 1730806752U;
        static const AkUniqueID GP_SPEAKER_HEALTH = 3112646011U;
        static const AkUniqueID GP_VOLUME_MASTER_SLIDERS = 13924921U;
        static const AkUniqueID GP_WOODBOX_HEALTH = 3533262112U;
    } // namespace GAME_PARAMETERS

    namespace TRIGGERS
    {
        static const AkUniqueID TEST = 3157003241U;
    } // namespace TRIGGERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MAIN_BANK = 3989171651U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID DELAY_ROOM2 = 282149398U;
        static const AkUniqueID REVERB_HP_MUSIC = 3676793708U;
        static const AkUniqueID REVERB_LASER = 1076479569U;
        static const AkUniqueID REVERB_PNJ = 2729821376U;
        static const AkUniqueID REVERB_PROPS = 508475202U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID DEFAULT_MOTION_DEVICE = 4230635974U;
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
